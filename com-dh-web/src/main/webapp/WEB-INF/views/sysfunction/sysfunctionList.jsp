<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
//        var $win;
        $(function(){
            $('#list_data').datagrid({
                title:"系统菜单列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"true"
            });
        });

        function _add(){

            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit');

        }

        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                _del(id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _del(id){
            $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                if (data) {
                    $.ajax({
                        type:"POST",
                        url:"delete",
                        data:{"id":id},
                        dataType:"json",
                        success:function(data){
                            $.messager.alert("操作提示", data.msg, "info", function () {
                                $('#list_data').datagrid('load',{});
                            });
                        },
                        error:function(e){
                            $.messager.alert("操作提示", "操作失败");
                        }
                    });
                }
            });
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                _upda(row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _upda(id){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit?id='+id);
            $win.window('open');
        }

        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }
        function formatStatus(val,row){
            console.info("选中行："+row.id)
            if(row.parentNode == 'root')  return '';
            var update = '  <a href="#" onclick="_upda(\'' + row.id + '\');" class="easyui-linkbutton">修改</a>';

            var del = '  <a href="#" onclick="_del(\'' + row.id + '\');" class="easyui-linkbutton">删除</a>';

            if (row.flag == 1){
                return '<a href="#" onclick="updateStatus(\''+row.id+'\', \'0\');" class="easyui-linkbutton" style="color:red;">禁用</a>'+update+del;

            } else {
               // return '<input type="button"  value=""/>';
                return '<a href="#"  onclick="updateStatus(\''+row.id+'\',\'1\');"  class="easyui-linkbutton" >启用</a>'+update+del;
            }
        }

        function cellStyler(value,row,index){
            if (value == 0){
                return 'background-color:#ffee00;';
            }
            if (value == 'root'){
                return 'background-color:red;';
            }
        }

        function formatStatusText(val,row){
            if (val==0){
                return '禁用';

            } else {
                return '启用';
            }
        }

        function _reload(){
            $("#list_data").datagrid("reload");
        }

        /**
         * 修改状态
         * */
        function updateStatus(id, flag){
            $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                if (data) {
                    $.ajax({
                        type:"POST",
                        url:"update_status",
                        data:{"id":id,"flag":flag},
                        dataType:"json",
                        success:function(data){
                            $.messager.alert("操作提示", data.msg, "info", function () {
                                $('#list_data').datagrid('load',{});
                            });
                        },
                        error:function(e){
                            $.messager.alert("操作提示", "操作失败");
                        }
                    });
                }
            });

        }

        function _function_edit(){
            showMyWindow(
                    "myWindow",
                    "功能编辑",
                    'function_edit',
                     800,
                     600);
        }

    </script>
</head>
<body style="padding: 0px;">
<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'north'" style="height:115px">
        <fieldset>
            <legend>信息查询</legend>
            <%--<form id="ffSearch" method="post">--%>
            <div >
                <table cellspacing="5" cellpadding="0">
                    <tr>
                        <td>
                            <label for="title">标题头：</label>
                        </td>
                        <td>
                            <input type="text" id="title"  name="txtProvinceName" style="width:100px" />
                        </td>
                        <td>
                            <label for="titleImg">图片地址：</label>
                        </td>
                        <td>
                            <input type="text" id="titleImg" name="titleImg" style="width:100px" />
                        </td>
                        <td>
                            <label for="flag">发布状态：</label>
                        </td>
                        <td>
                            <input type="text" id="flag" name="flag" style="width:100px" />
                        </td>
                        <td>
                            <label for="enableStatus">启用状态：</label>
                        </td>
                        <td>
                            <input type="text" id="enableStatus" name="enableStatus" style="width:100px" />
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <label>日期:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px; border-color: #ffffff;">
                        </td>
                        <td>
                            <label>至:</label>
                        </td>
                        <td>
                            <input class="easyui-datebox" style="width:110px;height: 30px;">
                        </td>
                        <td colspan="3" align="right">
                            <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnImport" iconcls="icon-package_add">导入</a>
                            <a href="javascript:void(0)" class="easyui-linkbutton" id="btnExport" iconcls="icon-package_go">导出</a>
                        </td>
                    </tr>
                </table>
            </div>
            <%--</form>--%>
        </fieldset>
    </div>
<div id="toolbar" style="padding:5px;height:auto">
    <div style="margin-bottom:5px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-pencil_delete" plain="true" onclick="_delete();" style="color: black;">删除</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-table" plain="true" onclick="_function_edit();" style="color: black;">功能查看</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="_reload();" style="color: black;">刷新</a>
    </div>
</div>
    <div data-options="region:'center'">
<table id="list_data">
    <thead>
    <tr>
       <%-- <th class="center" field="id"><input type="checkbox" class="select-all"></th>--%>
        <th field="id" hidden="true"  width="120" >编号</th>
        <th field="name" width="120" align="center">名称</th>
        <th field="imageUrl" width="120"  align="center">图片地址</th>
        <th field="url" width="120"  align="center">跳转地址</th>
        <th field="qtip" width="120"  align="center">提示语</th>
        <th field="leaf" hidden="true" width="120">叶子节点</th>
        <th  data-options="field:'parentNode', width:120,align:'center',styler:cellStyler">父节点</th>
        <th field="createDate" width="120"  align="center">创建时间</th>
        <th data-options="field:'flag', width:80,align:'center',formatter:formatStatusText">状态</th>
        <th data-options="field:'opt', width:150,align:'center',formatter:formatStatus">操作</th>
    </tr>
    </thead>
    </table>
    </div>
    </div>
<div id="myWindow" iconCls="icon-save" title="My Window"></div>
</body>
</html>