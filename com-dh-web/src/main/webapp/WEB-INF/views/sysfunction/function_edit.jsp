<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%@include file="/WEB-INF/views/common/taglib.jsp" %>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>

<style>
  li{
    float: none;
    list-style-type: none;
    padding: 5px;
  }
  li a{
    width: 50px;
  }
  ul{
    padding: 0px;
  }

  .panel-body {
    border-color: #d19405;
  }
</style>
<script type="text/javascript">

  $(function(){
    $('#selected_table').datagrid({
      class:"easyui-datagrid",
      url:"searched",
      rownumbers:"true",
      border:"0"
    });


    $('#no_selected_table').datagrid({
      class:"easyui-datagrid",
      url:"nosearched",
      rownumbers:"true",
      border:"0"
    });

    $("input[type='radio']").change(function(){
//      alert(this.value);
      searchButton(this.value);
    });


  });
  function searchButton(id){
    $('#selected_table').datagrid('load',{id: id});
    $('#no_selected_table').datagrid('load',{id: id});
  }


  function save(){

    $.ajax({
      type:"POST",
      url:"save",
      data:$("#editForm").serialize(),
      dataType:"json",
      success:function(data){
        $.messager.alert("操作提示", data.msg, "info", function () {
          parent.$('#myWindow').window('close');
          parent.$('#list_data').datagrid('load',{});
        });
      },
      error:function(e){

        $.messager.alert("操作提示", "操作失败");
      }
    });
  }

  function select_right(){
    var rows = $('#no_selected_table').datagrid('getSelections');
    if(rows != null && rows.length>0){
      select_used_cancel(rows, 1);
    }else{
      $.messager.alert("操作提示", "请选择一条数据");
    }
  }

  function select_all_right(){
    var rows = $('#no_selected_table').datagrid('getSelections');
    if(rows != null && rows.length>0){
      select_used_cancel(rows,1);
    }else{
      $.messager.alert("操作提示", "请选择一条数据");
    }
  }

  function select_left(){
    var rows = $('#selected_table').datagrid('getSelections');
    if(rows != null && rows.length>0){
      select_used_cancel(rows,0);
    }else{
      $.messager.alert("操作提示", "请选择一条数据");
    }
  }

  function select_all_left(){
    var rows = $('#selected_table').datagrid('getSelections');
    if(rows != null && rows.length>0){
      select_used_cancel(rows,0);
    }else{
      $.messager.alert("操作提示", "请选择一条数据");
    }
  }

  function cancel(){
    $('#win').window('close');
  }


  function select_used_cancel(rows,flag){

    var list = new Array();
    for(var i = 0; i<rows.length;i++){
      list.push(rows[i].id);
    }
    var parentNode = $("input[type='radio']:checked").val();
    if(parentNode!=null) {
      $.ajax({
        type: "POST",
        url: "select_used_cancel",
        data: {"rows": list, "parentNode":parentNode, "flag": flag},
        dataType: "json",
        success: function (data) {
          $.messager.alert("操作提示", data.msg, "info", function () {
              searchButton(parentNode);
          });
        },
        error: function (e) {
          $.messager.alert("操作提示", "操作失败");
        }
      });
    }
  }


</script>
<div style="height: 13%; margin: 0px; text-align: center; padding-left: 20px; padding-right: 20px; padding-top: 0px;">
    <fieldset style="border: 1px solid #d19405;">
      <legend align="left" style="font-weight: bold;">功能维护</legend>
      <div >
        <table cellspacing="1" cellpadding="0" style="font-size: 12px;">
          <tr>
            <td>
              <label>父菜单：</label>
            </td>
            <td>
              <c:forEach var="item" items="${list}" varStatus="status">
                <input type="radio" name="qx" value="${item.id}"/>${item.name}
              </c:forEach>
            </td>
          </tr>
        </table>
      </div>
    </fieldset>
</div>
<div style="height: 78%; margin: 0px; text-align: center; padding-left: 20px; padding-right: 20px; padding-top: 0px;">
    <div style="float: left; text-align: center;">
      <table id="no_selected_table" style="width:300px;height:420px;">
        <thead>
        <tr>
          <th field="id" hidden="true">编号</th>
          <th field="ck" checkbox="true"></th>
          <th field="name" width="237" align="center">待选功能</th>
        </tr>
        </thead>
      </table>
    </div>

    <div style="float: left; text-align: left; margin-left: 30px; line-height: 30px;">
      <ul>
        <li><a href="javascript:void(0)" class="easyui-linkbutton" onclick="select_right()">&gt;</a></li>
        <li><a href="javascript:void(0)" class="easyui-linkbutton" onclick="select_all_right()">&gt;&gt;</a></li>
        <li><a href="javascript:void(0)" class="easyui-linkbutton" onclick="select_left()">&lt;</a></li>
        <li><a href="javascript:void(0)" class="easyui-linkbutton" onclick="select_all_left()">&lt;&lt;</a></li>
      </ul>
    </div>

    <div style="float: right; margin-left: 20px;">
      <table id="selected_table"  style="width:300px;height:420px;">
        <thead>
        <tr>
          <th field="id" hidden="true">编号</th>
          <th field="ck" checkbox="true"></th>
          <th field="name" width="237" align="center">已选功能</th>
        </tr>
        </thead>
      </table>
    </div>
</div>
<div id="dlg-buttons" style="text-align: right;padding-left: 20px; padding-right: 20px; margin-top: 10px;">
  <%--<a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>--%>
  <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>