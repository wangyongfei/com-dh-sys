<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%@include file="/WEB-INF/views/common/taglib.jsp" %>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>

<script type="text/javascript">
    function save(){

        $.ajax({
            type:"POST",
            url:"save",
            data:$("#editForm").serialize(),
            dataType:"json",
            success:function(data){
                $.messager.alert("操作提示", data.msg, "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                });
            },
            error:function(e){

                $.messager.alert("操作提示", "操作失败");
            }
        });
    }

    function validForm(){
      //  $.messager.alert("操作提示", "请选择一条数据");
    }

    function cancel(){
        $('#win').window('close');
    }
</script>
<div style="height: 91%; margin: 0px; text-align: center; padding-top: 20px;">
    <form id="editForm" method="post">
        <input type="hidden" value="${vo.id}" name="id" id="id"/>
        <table align="center" style="font-size: 12px; padding: 0px;">
            <tr>
                <td>
                    <label>名称</label>
                </td>
                <td>
                    <input type="text" name="name" id="name"  value="${vo.name}" placeholder="名称"/>
                </td>
            </tr>
             <tr>
                <td>
                    <label>图片地址</label>
                </td>
                <td>
                    <input type="text" name="imageUrl" id="imageUrl"  value="${vo.imageUrl}" placeholder="图片地址" />
                </td>
            </tr>
            <tr>
                 <td>
                     <label>跳转地址</label>
                 </td>
                 <td>
                     <input type="text" name="url" id="url"  value="${vo.url}" placeholder="跳转地址"/>
                 </td>
             </tr>
            <tr>
               <td>
                   <label>提示语</label>
               </td>
               <td>
                   <input type="text" name="qtip" id="qtip"  value="${vo.qtip}" placeholder="提示语"/>
               </td>
           </tr>
            <tr>
                <td>
                    <label>叶子节点</label>
                </td>
                <td>
                    <%--<input type="text" name="leaf" id="leaf"  value="${vo.leaf}" placeholder="叶子节点"/>--%>
                    <select id="leaf"  name="leaf">
                        <option value="0">否</option>
                        <option value="1">是</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>父节点</label>
                </td>
                <td>
                    <%--<input type="text" name="parentNodeNum" id="parentNodeNum"  value="${vo.parentNode}" placeholder="父节点"/>--%>
                    <select  name="parentNodeNum" id="parentNodeNum">
                        <c:forEach var="item" items="${list}" varStatus="status">
                            <option value="${item.id}">${item.name}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
             <tr>
                  <td>
                      <label>是否启用</label>
                  </td>
                  <td>
                      <code:select id="flag" name="flag" codeType="status"></code:select>
                      <%--<input name="flag" id="flag"  value="${vo.flag}" placeholder="是否启用"/>--%>
                  </td>
              </tr>
      </table>
    </form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>