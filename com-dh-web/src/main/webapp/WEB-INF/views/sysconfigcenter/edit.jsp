<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/base.jsp"%>
<%@include file="/WEB-INF/views/common/taglib.jsp" %>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
<style>
    .jf-PropertyName{
        color: #CC0000;
    }
    .jf-String{
        color: #007777;
    }
    #josn_div{
        font-family: Lucida Console,Georgia;
        color: #000000;
        border: solid 1px #CECECE;
        width: 100%;
        background-color: #FFFFDD;
    }
</style>
<script type="text/javascript">
    var jf = null; //创建对象

    $(function(){
        var options = {
            dom : '#jsonStr', //对应容器的css选择器
            baseDom : "jsonStr"
        };
        jf = new JsonFormater(options); //创建对象

        var param =  JSON.stringify(${vo.jsonStr});
        jf.baseFomat(param);
    });


    function save(){
        $.ajax({
            type:"POST",
            url:"save",
            data:$("#editForm").serialize(),
            dataType:"json",
            success:function(data){
                $.messager.alert("操作提示", data.msg, "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                });
            },
            error:function(e){
                $.messager.alert("操作提示", "操作失败");
            }
        });
    }

    function cancel(){
        parent.$('#myWindow').window('close');
    }

    function _JsonFormat(){
        var param = $("#jsonStr").val();
        try{
            JSON.parse(param);
            $("#jsonStr").val("");
            jf.baseFomat(param);
        }catch(e){
            $.messager.alert("操作提示","json格式不正确");
        }

    }

    function _JsonZip(){
        var param = $("#jsonStr").val();
        var re = param.replace(/\s+/g,"");//删除所有空格;
        try{
            JSON.parse(param);
            $("#jsonStr").val(re);
        }catch(e){
            $.messager.alert("操作提示","json格式不正确");
        }

    }
//
//    function _JsonHighLight(){
//        jf.doFormat($("#jsonStr").val()); //格式化json
//    }

</script>
<div style="height: 91%; margin: 0px; text-align: center; padding-top: 20px;">
    <form id="editForm" method="post">
        <input type="hidden" name="id" id="id"  value="${vo.id}"/>
        <table align="center" style="font-size: 12px; padding: 0px;">
            <tr>
                <td>
                    <label>系统类型</label>
                </td>
                <td>
                    <code:select id="sysType" name="sysType" codeType="sys_type"></code:select>
                    <%--<input type="text" name="sysType" id="sysType"  value="${vo.sysType}"/>--%>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>描述</label>
                </td>
                <td>
                    <input type="text" name="describeContent" id="describeContent"  value="${vo.describeContent}"/>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="_JsonFormat()" >JSON格式转换</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="_JsonZip()" >压缩</a>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>JSON串</label>
                </td>
                <td>
                    <textarea  name="jsonStr" id="jsonStr" cols="50" rows="20" style="background-color: #263238; font-weight:bold ;  color: #A5E88D;  font-family:  "Source Sans Pro", 'Microsoft Yahei', '微软雅黑', sans-serif, Helvetica, 'Hiragino Sans GB', Arial;"></textarea>

                    <%--<input type="text" name="jsonStr" id="jsonStr"  value="${vo.jsonStr}"/>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <label>参数设置</label>
                </td>
                <td>
                    <textarea  name="paramSet" id="paramSet">${vo.paramSet}</textarea>
                    <%--<input type="text" name="paramSet" id="paramSet"  value=/>--%>
                </td>
            </tr>
                    </table>
    </form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>