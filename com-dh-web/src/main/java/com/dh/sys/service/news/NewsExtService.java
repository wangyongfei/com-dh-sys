package com.dh.sys.service.news;

import com.dh.sys.news.dao.ext.DhNewsExtDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2016/9/16.
 */

@Service
public class NewsExtService {

    @Autowired
    private DhNewsExtDaoImpl dhNewsExtDao;
    public String getMaxCodeNews(){
        return dhNewsExtDao.getMaxCodeNews();
    }
}
