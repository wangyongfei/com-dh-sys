package com.dh.sys.service.thread;

import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.service.SysSyncService;
import com.dh.sys.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2017/1/7.
 */
public class Consumer implements Runnable{

    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private BlockingQueue<List<SysSync>> queue;

    private SysSyncService service;

    public Consumer(BlockingQueue<List<SysSync>> queue,SysSyncService service) {
        this.queue = queue;
        this.service = service;
    }

    @Override
    public void run() {
       boolean flag = true;
       while (flag){
           try {
               List<SysSync>list = queue.poll(2, TimeUnit.SECONDS);
               if(null != list && !list.isEmpty()){
                   LOGGER.info("执行线程:{}, 执行大小:{}",Thread.currentThread().getName(),list.size());
                    for(SysSync sysSync:list) {
                        sysSync.setOptDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
                        sysSync.setCallCount(sysSync.getCallCount() + 1);
                        sysSync.setFinishDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
                        sysSync.setStatus(0);
                        service.update(sysSync);
                    }
               }

           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
    }
}
