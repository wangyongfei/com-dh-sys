package com.dh.sys.controller.news;

import com.alibaba.fastjson.JSON;
import com.dh.sys.mongo.FilePicsEntity;
import com.dh.sys.mongo.MongoService;
import com.dh.sys.news.model.DhNews;
import com.dh.sys.news.service.DhNewsService;
import com.dh.sys.service.news.NewsExtService;
import com.dh.sys.util.AutoGenerationCode;
import com.dh.sys.util.DhUtil;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


//@Slf4j
@Controller
public class DhNewsController {

    @Autowired
    private DhNewsService service;

    @Autowired
    private NewsExtService newsExtService;

    @Autowired
    private MongoService mongoService;

    @Value("${mongobase.mongofs.dh.mongoImgPath}")
    private String imgPath;

    @RequestMapping("/news/searchpage")
    public ModelAndView searchPage(){
        return  new ModelAndView("news/list");
    }

    @RequestMapping("/news/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        DhNews record = new DhNews();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<DhNews> list = service.findAllListPage(record,page,rows); //获取所有用户数据
        int total = service.count(record);
        List<Object>listObj = new ArrayList<Object>();
        for(int i = 0; i<list.size(); i++){
                JsonObject object=new JsonObject();
                object.addProperty("id", list.get(i).getId());
                object.addProperty("title", list.get(i).getTitle());
                object.addProperty("titleImg", imgPath+list.get(i).getTitleImg());
                object.addProperty("createDate", list.get(i).getCreateDate());
                object.addProperty("updateDate", list.get(i).getUpdateDate());
                object.addProperty("deleteFlag", list.get(i).getDeleteFlag());
                object.addProperty("flag", list.get(i).getFlag());
                object.addProperty("enableStatus", list.get(i).getEnableStatus());
                if (list.get(i).getNewContent().length>0) {
                    object.addProperty("newContent", new String(list.get(i).getNewContent()));
                }
                listObj.add(object);
        }


        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(listObj, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/news/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") String id){

        ModelAndView modelAndView = new ModelAndView("news/edit");

        if(!StringUtil.isNullOrEmpty(id)){
            DhNews record = service.selectById(id);
            record.setTitleImg(imgPath+record.getTitleImg());
            FilePicsEntity filePicsEntity =mongoService.findById(id);
            if(null != filePicsEntity) {
                record.setNewContent(filePicsEntity.getContent());
                modelAndView.addObject("newscontent", new String(filePicsEntity.getContent()));
            }
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/news/delete")
    @ResponseBody
    public void delete(
                        DhNews vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            mongoService.remove(vo.getId());
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/news/save")
    public void save(
                        DhNews vo,
                        String newscontent,
                        HttpServletRequest request,
                        HttpServletResponse response
                       ){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        DhNews record = new DhNews();

        record.setDeleteFlag(1);
        record.setFlag(1);
        record.setEnableStatus(1);
        record.setTitle(vo.getTitle());
        record.setNewContent(newscontent.getBytes());
        FilePicsEntity filePicsEntity = new FilePicsEntity();

        if(StringUtil.isNullOrEmpty(vo.getId())){
            record.setId(DhUtil.getUUID());
        }else{
            record.setId(vo.getId());
            mongoService.remove(record.getId());
        }

        filePicsEntity.setId(record.getId());
        filePicsEntity.setContent(newscontent.getBytes());
        filePicsEntity.setCreateDate(new Date());
        mongoService.save(filePicsEntity);



        if (multipartResolver.isMultipart(request)) {
            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
//            record.setId(DhUtil.getUUID());
            Iterator<String> iter = multiRequest.getFileNames();
            while (iter.hasNext()){
                MultipartFile file = multiRequest.getFile((String)iter.next());
                if (file.getSize() > 0){
                    record.setTitleImg(record.getId()+".jpg");
                    try {

                        mongoService.store(file.getInputStream(), record.getTitleImg());
                        System.out.println("图片保存成功");
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            if(StringUtil.isNullOrEmpty(vo.getId())){
                record.setCreateDate(StringUtil.getCurrLongDate());
                record.setUpdateDate(StringUtil.getCurrLongDate());
                String maxCode = newsExtService.getMaxCodeNews();
                record.setNewsCode(AutoGenerationCode.autoGeneration8Code(maxCode,"N"));
                service.save(record);
            }else{
                record.setUpdateDate(StringUtil.getCurrLongDate());
                service.update(record);
            }
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

//        return  new ModelAndView("news/list");
//        return "redirect:/news/search";
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }


    @RequestMapping(value="/news/saveRedis")
    public void saveRedis(
            HttpServletRequest request,
            HttpServletResponse response
    ) {

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            DhNews dhNews = new DhNews();
            dhNews.setId("N10000000000000000000001");
            dhNews.setTitle("测试redis缓存数据");
            service.saveRedis(dhNews);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/news/getRedis")
    public void getRedis(
            @RequestParam("id") String id,
            HttpServletRequest request,
            HttpServletResponse response
    ) {

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            DhNews dhNews = service.getByKey(id);
//            log.info("redis 返回内容{}", JSON.toJSONString(dhNews));
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
