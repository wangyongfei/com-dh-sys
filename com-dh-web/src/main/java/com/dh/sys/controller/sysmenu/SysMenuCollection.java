package com.dh.sys.controller.sysmenu;


import com.dh.sys.sysmenu.model.SysMenu;

import java.util.List;

public class SysMenuCollection{

	private SysMenu sysMenu;

	public SysMenu getSysMenu() {
		return sysMenu;
	}

	public void setSysMenu(SysMenu sysMenu) {
		this.sysMenu = sysMenu;
	}

	private List<SysMenu> list;

	public List<SysMenu> getList() {
		return list;
	}

	public void setList(List<SysMenu> list) {
		this.list = list;
	}
}
