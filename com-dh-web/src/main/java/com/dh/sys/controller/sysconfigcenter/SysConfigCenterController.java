package com.dh.sys.controller.sysconfigcenter;

import com.dh.sys.mongo.MongoService;
import com.dh.sys.sysconfigcenter.model.SysConfigCenter;
import com.dh.sys.sysconfigcenter.service.SysConfigCenterService;
import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.util.DhUtil;
import com.dh.sys.util.StringUtil;
import com.dh.sys.util.XmlConverUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Date;


@Controller
public class SysConfigCenterController {

    @Autowired
    private SysConfigCenterService service;

    @Autowired
    private MongoService mongoService;

    @RequestMapping("/sysconfigcenter/searchpage")
    public ModelAndView searchPage(){
        return  new ModelAndView("sysconfigcenter/list");
    }

    @RequestMapping("/sysconfigcenter/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        SysConfigCenter record = new SysConfigCenter();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<SysConfigCenter> list = service.findAllListPage(record,page,rows);
        int total = service.count(record);

        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(list, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/sysconfigcenter/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") String id){

        ModelAndView modelAndView = new ModelAndView("sysconfigcenter/edit");

        if(!StringUtil.isNullOrEmpty(id)){
            SysConfigCenter record = service.selectById(id);
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/sysconfigcenter/delete")
    @ResponseBody
    public void delete(
                        SysConfigCenter vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/sysconfigcenter/save")
    @ResponseBody
    public void save(
                        SysConfigCenter vo,
                        HttpServletResponse response,HttpSession httpSession){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            PrintWriter out = response.getWriter();
            SysUser sysUser = (SysUser) httpSession.getAttribute("system_user");
            if(null == sysUser){
                out.write(returnMsg("操作失败",1));
                return;
            }

            if(StringUtil.isNullOrEmpty(vo.getId())){
                vo.setId(DhUtil.getUUID());
                vo.setCreateDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
                vo.setUpdateDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
                vo.setCreateUser(sysUser.getUserName());
                vo.setUpdateUser(sysUser.getUserName());
                service.save(vo);
            }else{
                vo.setUpdateDate(StringUtil.getDateByFormat(new Date(), StringUtil.LONG_DATE_FMT));
                vo.setUpdateUser(sysUser.getUserName());
                service.update(vo);
            }

            out.write(returnMsg("操作成功",0));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/sysconfigcenter/json2Xml")
    @ResponseBody
    public void json2Xml(
            SysConfigCenter vo,
            HttpServletResponse response,HttpSession httpSession){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            PrintWriter out = response.getWriter();
            if(!StringUtil.isNullOrEmpty(vo.getId())){
                SysConfigCenter sysConfigCenter = service.selectById(vo.getId());
                Json2Xml json2Xml = new Json2Xml();
                json2Xml.setId(vo.getId());
                json2Xml.setXml(XmlConverUtil.jsontoXml(sysConfigCenter.getJsonStr().replaceAll("[\\t\\n\\r]", "").replaceAll("\\s*", "")));
                json2Xml.setDate(new Date());
                mongoService.save(json2Xml);
                out.write(returnMsg("操作成功",0));
                return;
            }
            out.write(returnMsg("操作失败",1));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }
    }
