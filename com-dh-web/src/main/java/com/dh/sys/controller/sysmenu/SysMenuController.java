package com.dh.sys.controller.sysmenu;

import com.dh.sys.sysmenu.model.SysMenu;
import com.dh.sys.sysmenu.service.SysMenuService;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;


@Controller
public class SysMenuController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysMenuController.class);
	
	@Autowired
	private SysMenuService sysMenuService;

	@RequestMapping("/sysmenu/searchpage")
	public ModelAndView sysfunctionSearch(){
//		System.out.println("跳转至header...");
		LOGGER.info("sysfunctionSearch菜单查询");
		return  new ModelAndView("sysfunction/sysfunctionList");
	}

	@RequestMapping("/sysmenu/search")
	@ResponseBody
	public void search(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value="page", defaultValue="1") Integer page,
			@RequestParam(value="rows", defaultValue="10")  Integer rows){

		response.setCharacterEncoding("UTF-8");  
		response.setContentType("application/json; charset=utf-8"); 
        SysMenu record = new SysMenu();
		if(page>=1){
			page = (page-1)*rows;
		}
        List<SysMenu> list = sysMenuService.findAllListPage(record,page,rows); //获取所有用户数据
		int total = sysMenuService.count(record);

		PrintWriter out = null;  
	    try {  
	        out = response.getWriter();  
	        out.append(pageListToJson(list, total));
	        
	    } catch (IOException e) {  
	        e.printStackTrace();  
	    } finally {  
	        if (out != null) {  
	            out.close();  
	        }  
	    }  
		
	}

	@RequestMapping("/sysmenu/searched")
	@ResponseBody
	public void search(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value="id", defaultValue="") String id
			){

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		SysMenu record = new SysMenu();
		record.setParentNode(id);
		List<SysMenu> list = sysMenuService.selectSysMenu(record); //获取所有用户数据
		sysMenuService.selectSysMenu(record);
		int total = sysMenuService.count(record);

		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.append(pageListToJson(list, total));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}

	}


	@RequestMapping("/sysmenu/select_used_cancel")
	@ResponseBody
	public void select_used_cancel(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value="rows[]") List<String>rows,
			@RequestParam(value="parentNode") String parentNode,
			@RequestParam(value="flag") String flag
	){

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		try {

			for(String str: rows){
				SysMenu sysMenu = sysMenuService.selectById(str);
//				sysMenu.setId(str);
				if("1".equalsIgnoreCase(flag)) {
					sysMenu.setParentNode(parentNode);
				}else{
					sysMenu.setParentNode("");
				}
				sysMenuService.update(sysMenu);
			}
//			LogUtil.info("菜单删除结束"+flag);
			PrintWriter out = response.getWriter();

			out.write(returnMsg("修改成功",0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@RequestMapping("/sysmenu/nosearched")
	@ResponseBody
	public void nosearched(
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value="id", defaultValue="") String id
	){

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		SysMenu record = new SysMenu();
		record.setParentNode(id);
		List<SysMenu> list = sysMenuService.selectSysMenuNoContain(record); //获取所有用户数据
		sysMenuService.selectSysMenu(record);
		int total = sysMenuService.count(record);

		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.append(pageListToJson(list, total));

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}

	}

	@RequestMapping(value="/sysmenu/edit")
	public ModelAndView edit(HttpServletRequest request,
					 HttpServletResponse response,
					 @RequestParam(value="id", defaultValue="") String id){

		ModelAndView modelAndView = new ModelAndView("sysfunction/edit");
		SysMenu record = new SysMenu();
		record.setParentNode(String.valueOf(0));
		List<SysMenu> list = sysMenuService.selectSysMenu(record);
		if(!StringUtil.isNullOrEmpty(id)){
			SysMenu sysMenu = sysMenuService.selectById(id);
			modelAndView.addObject("vo", sysMenu);
		}
		modelAndView.addObject("list", list);
		return modelAndView;

	}

	@RequestMapping(value="/sysmenu/function_edit")
	public ModelAndView function_edit(HttpServletRequest request,
							 HttpServletResponse response,
							 @RequestParam(value="id", defaultValue="") String id){

		ModelAndView modelAndView = new ModelAndView("sysfunction/function_edit");
		SysMenu record = new SysMenu();
		record.setParentNode(String.valueOf(0));
		List<SysMenu> list = sysMenuService.selectSysMenu(record);
//		if(!StringUtil.isNullOrEmpty(id)){
//			SysMenu sysMenu = sysMenuService.selectById(id);
//			modelAndView.addObject("vo", sysMenu);
//		}
		modelAndView.addObject("list", list);
		return modelAndView;

	}
	
	@RequestMapping(value="/sysmenu/delete")
	@ResponseBody
	public void delete(SysMenu sm, HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");

		 try {
			 LOGGER.info("菜单删除"+sm.getId());
			 boolean flag =  sysMenuService.delete(sm);
			 LOGGER.info("菜单删除结束"+flag);
			 PrintWriter out = response.getWriter();

			 out.write(returnMsg("删除成功",0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value="/sysmenu/update_status")
	@ResponseBody
	public void updatetatus(SysMenu sm, HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");

		try {
			LOGGER.info("修改状态"+sm.getId());
			SysMenu sysMenu = sysMenuService.selectById(sm.getId());
			sysMenu.setFlag(sm.getFlag());
			boolean flag = sysMenuService.update(sysMenu);
			LOGGER.info("菜单修改状态结束"+flag);
			PrintWriter out = response.getWriter();

			out.write(returnMsg("修改成功",0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	
	@RequestMapping(value="/sysmenu/save")
	@ResponseBody
	public void save(SysMenu vo,
					 HttpServletResponse response,
					 @RequestParam(value="parentNodeNum", defaultValue="0") String parentNodeNum
					 ){

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");

		try {
			vo.setParentNode(parentNodeNum);
			if(StringUtil.isNullOrEmpty(vo.getId())){
				vo.setId(StringUtil.getUUID());
				vo.setCreateDate(StringUtil.getDateByFormat(new Date()));
				sysMenuService.save(vo);
			}else{
				SysMenu sysMenu = sysMenuService.selectById(vo.getId());
				vo.setCreateDate(sysMenu.getCreateDate());
				sysMenuService.update(vo);
			}
			 PrintWriter out = response.getWriter();
			 out.write(returnMsg("操作成功",0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@RequestMapping(value="/mainMenuSearch", method=RequestMethod.POST)
	@ResponseBody
	public void mainMenuSearch(HttpServletResponse response){
//		 try {
//			 response.setCharacterEncoding("UTF-8");
//			 response.setContentType("application/json; charset=utf-8");
//	         SysMenu record = new SysMenu();
//	         record.setFlag(1);//查询以启用功能
//	         List<SysMenu> list = sysMenuService.selectSysMenu(record); //获取所有用户数据
//			 int total = list.size();
//			 if(list != null && list.size() > 0){
//				 List<SysMenuCollection>listColl = new ArrayList<SysMenuCollection>();
//				 for (SysMenu sm : list) {
//					if(sm.getParentNode() == 0){
//						SysMenuCollection smc = new SysMenuCollection();
//						smc.setSysMenu(sm);
//						List<SysMenu>smcList = new ArrayList<SysMenu>();
//						for (SysMenu smChild : list) {
//							if(smChild.getParentNode() != 0 && Integer.parseInt(sm.getId())==smChild.getParentNode()){//子节点
//								smcList.add(smChild);
//							}
//						}
//						smc.setList(smcList);
//						listColl.add(smc);
//					}
//				 }
//
//				 PrintWriter out = response.getWriter();
//				 out = response.getWriter();
//			     out.append(pageListToJson2(listColl, total));
////				 out.write("true");
//			 }
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private String pageListToJson(List<?> list, int total){
        
        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

	private String pageListToJson2(List list, int total){

		JsonObject object=new JsonObject();
		Gson gson=new Gson();
		int count=list.size();
		//list.get(0).setCreateDate(null);
		JsonElement jsonElement=gson.toJsonTree(list);
		//items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
		object.add("list", jsonElement);
		object.addProperty("total", total);
		return object.toString();
	}

	private String returnMsg(String msg, Integer flag){
		JsonObject object=new JsonObject();
		object.addProperty("msg", msg);
		object.addProperty("flag", flag);
		return object.toString();
	}
}
