package com.dh.sys.controller.syslog;

import com.dh.sys.mongo.MongoService;
import com.dh.sys.sync.MongoSyncService;
import com.dh.sys.syslog.model.SysLog;
import com.dh.sys.syslog.service.SysLogService;
import com.dh.sys.util.DateUtils;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Controller
public class SysLogController {

    @Autowired
    private SysLogService service;

    @Autowired
    private MongoSyncService mongoSyncService;

    @Autowired
    private MongoService mongoService;

    @RequestMapping("/syslog/searchpage")
    public ModelAndView searchPage(){
        return  new ModelAndView("syslog/list");
    }

    @RequestMapping("/syslog/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        SysLog record = new SysLog();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<SysLog> list = service.findAllListPage(record,page,rows);
        int total = service.count(record);

        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(list, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/syslog/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") String id){

        ModelAndView modelAndView = new ModelAndView("syslog/edit");

        if(!StringUtil.isNullOrEmpty(id)){
            SysLog record = service.selectById(id);
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/syslog/delete")
    @ResponseBody
    public void delete(
                        SysLog vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/syslog/save")
    @ResponseBody
    public void save(
                        SysLog vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            if(StringUtil.isNullOrEmpty(vo.getId())){
                service.save(vo);
            }else{
                service.update(vo);
            }
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 同步T-1天数据
     * @param response
     */
    @RequestMapping(value="/syslog/mongoSync")
    @ResponseBody
    public void mongoSync(HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
          //  SysLog sysLog = new SysLog();
            String startDate = new StringBuffer().append(DateUtils.formatDate(calendar.getTime(), DateUtils.YYYYMMDD)).append(" 00:00:00").toString();
            String endDate =new StringBuffer().append(DateUtils.formatDate(new Date(), DateUtils.YYYYMMDD)).append(" 00:00:00").toString();
            //sysLog.setCreateDate(strDate);
            List<SysLog>list = service.select(startDate,endDate);
            if(null != list && !list.isEmpty()){
               String collectionName =  "sysLog_"+DateUtils.getYearAndSeason(startDate);
               mongoSyncService.insertCollection(collectionName);
                List<SysLog>list2 = new ArrayList<>();
                for(SysLog sysLog1: list ) {
                    mongoService.findAllList(sysLog1,collectionName);
                }
                mongoService.batchInsertByCollectionName(list,collectionName);
            }

            PrintWriter out = response.getWriter();
            out.write(returnMsg("同步成功",0));
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }
    }
