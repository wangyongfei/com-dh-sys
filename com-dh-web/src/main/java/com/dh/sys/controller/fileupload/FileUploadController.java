package com.dh.sys.controller.fileupload;

import com.dh.sys.mongo.FilePicsEntity;
import com.dh.sys.mongo.MongoService;
import com.dh.sys.util.DhUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
public class FileUploadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private MongoService mongoService;

    @Value("${mongobase.mongofs.dh.mongoImgPath}")
    private String savePath;

    @Value("${mongobase.mongofs.dh.mongoImgPath}")
    private String rootPath;
    /**
     * 文件上传
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return json response
     */
    @RequestMapping(value = "/fileupload/file_upload", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> fileUpload(HttpServletRequest request, HttpServletResponse response) {
//        //文件保存本地目录路径
//        String savePath = "http://www.fcworld.net/dhpics/";
//        //文件保存目录URL
//        String saveUrl = request.getContextPath() + savePath.substring(2);

        if(!ServletFileUpload.isMultipartContent(request)){
            return getError("请选择文件。");
        }
        //检查目录
        /*File uploadDir = new File(savePath);
        if(!uploadDir.isDirectory()){
            return getError("上传目录不存在。");
        }
        //检查目录写权限
        if(!uploadDir.canWrite()){
            return getError("上传目录没有写权限。");
        }*/

        String dirName = request.getParameter("dir");
        if (dirName == null) {
            dirName = "image";
        }

        //定义允许上传的文件扩展名
        Map<String, String> extMap = new HashMap<String, String>();
        extMap.put("image", "gif,jpg,jpeg,png,bmp");
        extMap.put("flash", "swf,flv");
        extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
        extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,xml,txt,zip,rar,gz,bz2");

        if(!extMap.containsKey(dirName)){
            return getError("目录名不正确。");
        }
//        //创建文件夹
//        savePath += dirName + "/";
//        saveUrl += dirName + "/";
//        File saveDirFile = new File(savePath);
//        if (!saveDirFile.exists()) {
//            saveDirFile.mkdirs();
//        }
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//        String ymd = sdf.format(new Date());
//        savePath += ymd + "/";
//        saveUrl += ymd + "/";
//        File dirFile = new File(savePath);
//        if (!dirFile.exists()) {
//            dirFile.mkdirs();
//        }
//
//        //最大文件大小
//        long maxSize = 1000000;

//        FileItemFactory factory = new DiskFileItemFactory();
//        ServletFileUpload upload = new ServletFileUpload(factory);
//        upload.setHeaderEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
//            record.setId(DhUtil.getUUID());
        Iterator<String> iter = multiRequest.getFileNames();
//        try {
//            items = upload.parseRequest(request);
//        } catch (FileUploadException fe) {
//            return getError("接收文件异常。");
//        }
//        Iterator<?> itr = items.iterator();
//        while (iter.hasNext()) {
            String newFileName = null;
            while (iter.hasNext()){
                MultipartFile file = multiRequest.getFile((String)iter.next());
                if (file.getSize() > 0){
                   // record.setTitleImg(record.getId()+".jpg");
                    try {
                        newFileName = DhUtil.getUUID()+".jpg";
                        mongoService.store(file.getInputStream(), newFileName);
                        System.out.println("图片保存成功");

                        Map<String, Object> succMap = new HashMap<String, Object>();

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            Map<String, Object> succMap = new HashMap<String, Object>();
            succMap.put("error", 0);
            succMap.put("url",  savePath+newFileName);
            return succMap;
//            FileItem item = (FileItem) itr.next();
//            MultipartFile file = multiRequest.getFile((String)iter.next());
//            String fileName = file.getName();
////            if (!file.isFormField()) {
//                //检查文件大小
//                if(file.getSize() > maxSize){
//                    return getError("上传文件大小超过限制。");
//                }
//                //检查扩展名
//                String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
//                if(!Arrays.<String>asList(extMap.get(dirName).split(",")).contains(fileExt)){
//                    return getError("上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
//                }
//
//                SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
//                String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
//                try{
//                    File uploadedFile = new File(savePath, newFileName);
//                    file.write(uploadedFile);
//                }catch(Exception e){
//                    return getError("上传文件失败。");
//                }
//
//                Map<String, Object> succMap = new HashMap<String, Object>();
//                succMap.put("error", 0);
//                succMap.put("url", saveUrl + newFileName);
//                return succMap;
//            }
//        }
    }

    private Map<String, Object> getError(String errorMsg) {
        Map<String, Object> errorMap = new HashMap<String, Object>();
        errorMap.put("error", 1);
        errorMap.put("message", errorMsg);
        return errorMap;
    }

    /**
     * 文件空间
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     * @return json
     */
    @RequestMapping(value = "/fileupload/fileManager")
    @ResponseBody
    public Object fileManager(HttpServletRequest request, HttpServletResponse response) {
        //根目录路径，可以指定绝对路径
//        String rootPath = "http://www.fcworld.net/dhpics/";
        //根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/
        String rootUrl  = rootPath;
        //图片扩展名
        String[] fileTypes = new String[]{"gif", "jpg", "jpeg", "png", "bmp"};

//        String dirName = request.getParameter("dir");
//        if (dirName != null) {
//            if(!Arrays.<String>asList(new String[]{"image", "flash", "media", "file"}).contains(dirName)){
//                return "Invalid Directory name.";
//            }
//            rootPath += dirName + "/";
//            rootUrl += dirName + "/";
//            File saveDirFile = new File(rootPath);
//            if (!saveDirFile.exists()) {
//                saveDirFile.mkdirs();
//            }
//        }
        //根据path参数，设置各路径和URL
        String path = request.getParameter("path") != null ? request.getParameter("path") : "";
        String currentPath = rootPath + path;
        String currentUrl = rootUrl + path;
        String currentDirPath = path;
        String moveupDirPath = "";
        if (!"".equals(path)) {
            String str = currentDirPath.substring(0, currentDirPath.length() - 1);
            moveupDirPath = str.lastIndexOf("/") >= 0 ? str.substring(0, str.lastIndexOf("/") + 1) : "";
        }

        //排序形式，name or size or type
        String order = request.getParameter("order") != null ? request.getParameter("order").toLowerCase() : "name";

        List<FilePicsEntity>list = mongoService.findAllList();


        //不允许使用..移动到上一级目录
//        if (path.indexOf("..") >= 0) {
//            return "Access is not allowed.";
//        }
//        //最后一个字符不是/
//        if (!"".equals(path) && !path.endsWith("/")) {
//            return "Parameter is not valid.";
//        }
//        //目录不存在或不是目录
//        File currentPathFile = new File(currentPath);
//        if(!currentPathFile.isDirectory()){
//            return "Directory does not exist.";
//        }
//
//        //遍历目录取的文件信息
        List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
        if(list != null) {
            for (FilePicsEntity file : list) {
                Hashtable<String, Object> hash = new Hashtable<String, Object>();
                String fileName = file.getId();
//                if(file.isDirectory()) {
//                    hash.put("is_dir", true);
//                    hash.put("has_file", (file.listFiles() != null));
//                    hash.put("filesize", 0L);
//                    hash.put("is_photo", false);
//                    hash.put("filetype", "");
//                } else if(file.isFile()){
                    String fileExt = "jpg";
                    hash.put("is_dir", false);
                    hash.put("has_file", false);
                    hash.put("filesize", 0);
                    hash.put("is_photo", Arrays.<String>asList(fileTypes).contains(fileExt));
                    hash.put("filetype", fileExt);
//                }
                hash.put("filename", fileName+"."+fileExt);
                hash.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.getCreateDate()));
                fileList.add(hash);
            }
        }

        if ("size".equals(order)) {
            Collections.sort(fileList, new SizeComparator());
        } else if ("type".equals(order)) {
            Collections.sort(fileList, new TypeComparator());
        } else {
            Collections.sort(fileList, new NameComparator());
        }
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("moveup_dir_path", moveupDirPath);
        result.put("current_dir_path", currentDirPath);
        result.put("current_url", currentUrl);
        result.put("total_count", fileList.size());
        result.put("file_list", fileList);

        return result;
    }

    private class NameComparator implements Comparator<Map<String, Object>> {
        public int compare(Map<String, Object> hashA, Map<String, Object> hashB) {
            if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
                return -1;
            } else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
                return 1;
            } else {
                return ((String)hashA.get("filename")).compareTo((String)hashB.get("filename"));
            }
        }
    }

    private class SizeComparator implements Comparator<Map<String, Object>> {
        public int compare(Map<String, Object> hashA, Map<String, Object> hashB) {
            if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
                return -1;
            } else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
                return 1;
            } else {
                if (((Long)hashA.get("filesize")) > ((Long)hashB.get("filesize"))) {
                    return 1;
                } else if (((Long)hashA.get("filesize")) < ((Long)hashB.get("filesize"))) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }

    private class TypeComparator implements Comparator<Map<String, Object>> {
        public int compare(Map<String, Object> hashA, Map<String, Object> hashB) {
            if (((Boolean)hashA.get("is_dir")) && !((Boolean)hashB.get("is_dir"))) {
                return -1;
            } else if (!((Boolean)hashA.get("is_dir")) && ((Boolean)hashB.get("is_dir"))) {
                return 1;
            } else {
                return ((String)hashA.get("filetype")).compareTo((String)hashB.get("filetype"));
            }
        }
    }

    private String pageListToJson(List<?> list){
        
        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("list", jsonElement);
//        object.addProperty("total", total);
        return object.toString();
    }
}
