package com.dh.sys.controller.sysuser;

import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.sysuser.service.SysUserService;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;


@Controller
public class SysUserController {

    @Autowired
    private SysUserService service;

    @RequestMapping("/sysuser/searchpage")
    public ModelAndView searchPage(){
        return  new ModelAndView("sysuser/list");
    }

    @RequestMapping("/sysuser/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        SysUser record = new SysUser();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<SysUser> list = service.findAllListPage(record,page,rows); //获取所有用户数据
        int total = service.count(record);

        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(list, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/sysuser/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") String id){

        ModelAndView modelAndView = new ModelAndView("sysuser/edit");

        if(!StringUtil.isNullOrEmpty(id)){
            SysUser record = service.selectById(id);
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/sysuser/delete")
    @ResponseBody
    public void delete(
                        SysUser vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/sysuser/save")
    @ResponseBody
    public void save(
                        SysUser vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            if(StringUtil.isNullOrEmpty(vo.getId())){
                vo.setId(StringUtil.getUUID());
                vo.setCreateDate(StringUtil.getDateByFormat(new Date()));
                service.save(vo);
            }else{
                service.update(vo);
            }
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        //list.get(0).setCreateDate(null);
        JsonElement jsonElement=gson.toJsonTree(list);
        //items为extjs store proxy reader中设置的root，即为数据源；totalCount为数据总数。
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }
    }
