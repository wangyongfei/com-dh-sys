package com.dh.sys.controller.userquota;

import com.dh.sys.userquota.model.UserQuota;
import com.dh.sys.userquota.service.UserQuotaService;
import com.dh.sys.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@Controller
public class UserQuotaController {

    @Autowired
    private UserQuotaService service;

    @RequestMapping("/userquota/searchpage")
    public ModelAndView searchPage(){
        return  new ModelAndView("userquota/list");
    }

    @RequestMapping("/userquota/search")
    @ResponseBody
    public void search(
        HttpServletRequest request,
        HttpServletResponse response,
        @RequestParam(value="page", defaultValue="1") Integer page,
        @RequestParam(value="rows", defaultValue="10")  Integer rows){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        UserQuota record = new UserQuota();
        if(page>=1){
            page = (page-1)*rows;
        }
        List<UserQuota> list = service.findAllListPage(record,page,rows);
        int total = service.count(record);

        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(pageListToJson(list, total));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    @RequestMapping(value="/userquota/edit")
    public ModelAndView edit(
                            HttpServletRequest request,
                            HttpServletResponse response,
                            @RequestParam(value="id", defaultValue="") Long id){

        ModelAndView modelAndView = new ModelAndView("userquota/edit");

        if(null != id){
            UserQuota record = service.selectById(id);
            modelAndView.addObject("vo", record);
        }
        return modelAndView;

    }

    @RequestMapping(value="/userquota/delete")
    @ResponseBody
    public void delete(
                        UserQuota vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {

            boolean flag =  service.delete(vo);
            PrintWriter out = response.getWriter();
            out.write(returnMsg("删除成功",0));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value="/userquota/save")
    @ResponseBody
    public void save(
                        UserQuota vo,
                        HttpServletResponse response){

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        try {
            if(null != vo.getId()){
                service.save(vo);
            }else{
                service.update(vo);
            }
            PrintWriter out = response.getWriter();
            out.write(returnMsg("操作成功",0));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String pageListToJson(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("rows", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String pageListToJson2(List list, int total){

        JsonObject object=new JsonObject();
        Gson gson=new Gson();
        int count=list.size();
        JsonElement jsonElement=gson.toJsonTree(list);
        object.add("list", jsonElement);
        object.addProperty("total", total);
        return object.toString();
    }

    private String returnMsg(String msg, Integer flag){
        JsonObject object=new JsonObject();
        object.addProperty("msg", msg);
        object.addProperty("flag", flag);
        return object.toString();
    }
    }
