package com.dh.sys.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

public class ServletUtils
{
  public static String parseRequest2String(HttpServletRequest request)
  {
    String ss = null;
    BufferedReader reader = null;
    try {
      System.out.println("编码格式:" + request.getCharacterEncoding());
      reader = new BufferedReader(new InputStreamReader(request.getInputStream(), request.getCharacterEncoding()));
      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        sb.append(line);
      }
      ss = sb.toString();
      System.out.println("Request获取数据：" + ss);
    }
    catch (IOException e) {
//      throw new Exception("000217", "2", null, null, e, null);
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
//        throw new IsbAppException("000217", "2", null, null, e, null);
      }
    }
    return ss;
  }
}
