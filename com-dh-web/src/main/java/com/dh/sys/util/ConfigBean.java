package com.dh.sys.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

/**
 * 待后续优化
 * 实现 FactoryBean<Object>
 * Created by Administrator on 2016/12/2.
 */
public class ConfigBean extends Properties implements InitializingBean{

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigBean.class);

    /**
     * 测试环境
     */
    private static final String PROFILES_LOCAL = "local";

    /**
     * 生产环境
     */
    private static final String PROFILES_PRODUCT = "product";

    /**
     * 超时设置
     */
    public static final int CONFIG_REMOTE_FETCH_TIMEOUT = 3000;

    /**
     * 加载配置环境
     */
    public CompositeConfiguration compositeConfiguration;

    private void getConfigNode(List<ConfigurationNode>list, String str){

        if(null != list && !list.isEmpty()){
            for(ConfigurationNode configurationNode:list){
                if(null != configurationNode.getChildren() && !configurationNode.getChildren().isEmpty()) {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append(str);
                    stringBuffer.append(configurationNode.getName());
                    stringBuffer.append(".");
                    getConfigNode(configurationNode.getChildren(),stringBuffer.toString());
                }else{
                    StringBuffer sb = new StringBuffer();
                    sb.append(str);
                    sb.append(configurationNode.getName());
                    this.setProperty(sb.toString(),configurationNode.getValue().toString());
                   // systemParam.put(sb.toString(), configurationNode.getValue());
                }
            }
        }
    }

    private String appName;

    private String hostName;

    private List<XMLConfiguration> configurations;

    public ConfigBean(){
    }

    public ConfigBean(Configuration configuration){
        this.compositeConfiguration = new CompositeConfiguration(configuration);
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;

    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;

    }

    public List<XMLConfiguration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<XMLConfiguration> configurations) {
        this.configurations = configurations;
    }

    /**
     * bean初始化时执行
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {

        if (compositeConfiguration == null) {
            compositeConfiguration = new CompositeConfiguration();
        }

        if(null != getConfigurations()){

            /**环境*/
            String profiles = System.getProperty("spring.profiles.active", "local");

            //加载配置资源
            if(PROFILES_LOCAL.equalsIgnoreCase(profiles)){
                getConfigNode(configurations.get(0).getRoot().getChildren(), "");
               // compositeConfiguration.addConfiguration(configurations.get(0));
            }else if(PROFILES_PRODUCT.equalsIgnoreCase(profiles)){
               //加载本地或远程配置文件
               XMLConfiguration xmlConfiguration = null;

               //判断本地是否有缓存
               String cacheFilePath = System.getProperty("spring.files.cacheFile", "local");
               if(StringUtil.isNullOrEmpty(cacheFilePath)){
                    throw new RuntimeException("local configer file no.");
               }

               String cacheFile = cacheFilePath + getAppName() + "_" + getHostName() + ".xml";
               File cachePath = new File(cacheFile);
               boolean cache = cachePath.exists();
                if (!cache) {
                    String api_url = System.getProperty("api.url");
                    String url = api_url + "/" + getHostName() + "_" + getAppName()+".xml";
                    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                        HttpGet httpget = new HttpGet(url);
                        RequestConfig defaultRequestConfig = RequestConfig.custom()
                                .setSocketTimeout(CONFIG_REMOTE_FETCH_TIMEOUT)
                                .setConnectTimeout(CONFIG_REMOTE_FETCH_TIMEOUT)
                                .setConnectionRequestTimeout(CONFIG_REMOTE_FETCH_TIMEOUT)
                                .setStaleConnectionCheckEnabled(true).build();
                        httpget.setConfig(defaultRequestConfig);
                        ResponseHandler<String> responseHandler = new BasicResponseHandler();
                        String responseBody = httpclient.execute(httpget, responseHandler);
                        xmlConfiguration = new XMLConfiguration();
                        xmlConfiguration.load(new StringReader(responseBody));

                        try {
                            File filePath = new File(cacheFilePath);
                           if (!filePath.exists()) {
                               FileUtils.forceMkdir(filePath);
                           }
                           xmlConfiguration.save(cacheFile);
                       } catch (Exception e) {
                           LOGGER.error("保存缓存配置文件出错:{}", cacheFile, e);
                           throw new RuntimeException("保存缓存配置文件出错:" + cacheFile);
                       }

                    } catch (Exception e) {
                        LOGGER.error("加载配置中心数据异常{}", url, e);
                        throw new RuntimeException("获取远程配置出错:{}" + url);
                    }
                }else{//有缓存，则直接用缓存
                    // 获取远程配置失败，尝试使用本地缓存
                    LOGGER.info("尝试用本地缓存:{}", cacheFile);
                    try {
                        xmlConfiguration = new XMLConfiguration(cacheFile);
                    } catch (ConfigurationException e1) {
                        LOGGER.error("获取本地缓存配置文件出错:", e1);
                        throw new RuntimeException("获取本地缓存配置文件出错:" + cacheFile);
                    }
                }

                if(null == xmlConfiguration){
                    throw new RuntimeException("获取本地缓存配置文件出错.");
                }
                getConfigNode(xmlConfiguration.getRoot().getChildren(), "");
                //compositeConfiguration.addConfiguration(xmlConfiguration);

           }else{
               LOGGER.info("配置中心无数据，请检查配置！！！");
               return;
           }
           // getConfigNode(list, "");
            LOGGER.info("配置中心数据启动完毕");
        }
    }

    public static String base64andMd5(String str) {
        try {
            byte[] baseres = Base64.encodeBase64(str.getBytes("UTF-8"));
            return  DigestUtils.md5Hex(baseres);
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        return StringUtils.EMPTY;
    }


}
