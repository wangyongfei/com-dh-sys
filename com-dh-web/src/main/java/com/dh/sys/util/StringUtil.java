package com.dh.sys.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @项目名称：srx-bbt-adapter-out
 * @类名称：StringUtil.java
 * @类描述：字符串处理公用类
 * @创建人：wyf
 * @创建时间：2014-11-14下午5:07:48
 * @修改人：wyf
 * @修改时间：2014-11-14下午5:07:48
 * @修改备注：
 * @版本：V1.0
 */
public class StringUtil {
	public static String LONG_DATE_FMT = "yyyy-MM-dd HH:mm:ss";
	public static String LONG1_DATE_FMT = "yyyy/MM/dd HH:mm:ss";
	public static String SHORT_DATE_FMT = "yyyy-MM-dd";

	/**
	 * 判断传入的字符串是否是 NULL 或“”
	 * @param s 字符串
	 * @return
	 */
	public static boolean isNullOrEmpty(String s)
	{
		if (s==null || "".equals(s.trim()) || "NULL".equals(s.toUpperCase()) || "UNDEFINED".equals(s.toUpperCase()))
		{
			return true;
		}
		return false;
	}
	public static int compareTo(String s1,String s2)
	{
		String str1=s1==null?"":s1;
		String str2=s2==null?"":s2;
		return str1.compareTo(str2);
	}
	/**
	 * 比对传入的两个字符串是否相等
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static boolean equals(String s1,String s2)
	{
		if (isNullOrEmpty(s1))
		{
			return isNullOrEmpty(s2);
		}
		else
		{
			return s1.equals(s2);
		}
	}
	
	/**
	 * @描述 判断字符串是否无效
	 * @param str
	 * @return 无效：true。 有效：false
	 * @author zzy 
	 * @创建时间 2014-6-24 
	 */
	public static boolean invalid(String... str){
		boolean flag=false;
		int i;
		if(str!=null && str.length>0){
			for(i=0;i<str.length;i++){
				if(str[i]==null || str[i].trim().equals(""))
					flag=true;
			}
		}
		return flag;
	}

	/**
	 * 判断一个日期字符串是否是指定的格式
	 * @param sDateStr 日期字符串
	 * @param sFormatStr 比对的格式化字符串
	 * @return
	 */
	public static boolean isDateFormat(String sDateStr,String sFormatStr){
		if(sDateStr==null) return false;
		if(sFormatStr==null || "".equals(sFormatStr.trim())) sFormatStr = SHORT_DATE_FMT;
		SimpleDateFormat formatter = new SimpleDateFormat(sFormatStr);
		try {
			formatter.parse(sDateStr);
		} catch (ParseException e) {
			//			e.printStackTrace();
			return false;
		}
		return true;
	}
	//	public static void main(String[] args) {
	//		//System.out.println(isDateFormat("",LONG_DATE_FMT));
	//		System.out.println(StringUtil.StringToDate("1987-06-12", SHORT_DATE_FMT));
	//	}


	/**
	 * 将传入的时间字符串 转化成指定格式的日期
	 * @param strDate 日期字符串
	 * @param strFmt 指定日期格式字符串 如 yyyy-MM-dd HH:mm:ss
	 * @return 转换后的日期
	 */
	public static Date StringToDate(String strDate,String strFmt)
	{
		if(isNullOrEmpty(strFmt))
			strFmt = SHORT_DATE_FMT;
		SimpleDateFormat formatDate = new SimpleDateFormat(strFmt);
		//String str = formatDate.format(strDate);
		Date time = null;
		try {
			time = formatDate.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time;
	}

	/**
	 * 获得指定日期N天后的日期
	 * @param sDateStr 指定日期
	 * @param iNum 值前或之后天数
	 * @return 计算后的日期字符串 短日期格式 yyyy-MM-dd
	 */
	@SuppressWarnings("static-access")
	public static String getAfterOrBeForDateStr(Date date,int iNum){
		if(date== null) date = new Date();
		java.util.Calendar  calendar = java.util.Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE,iNum);
		return getDateByFormat(calendar.getTime(), StringUtil.SHORT_DATE_FMT);
	}
	/**
	 * 根据格式字符串 获取指定时间格式字符串
	 * @param sFormatStr 格式字符串
	 * @return
	 */
	public static String getDateByFormat(String sFormatStr){
		if(sFormatStr==null || "".equals(sFormatStr.trim())) sFormatStr = LONG_DATE_FMT;
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(sFormatStr);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	/**
	 * 将传日的日期转换成指定日期格式的日期字符串 LONG_DATE_FMT
	 * @param date 要转换日期
	 * @return
	 */
	public static String getDateByFormat(Date date){
		if(date==null) return null;
		SimpleDateFormat formatter = new SimpleDateFormat(LONG1_DATE_FMT);
		String dateString = formatter.format(date);
		return dateString;
	}
	/**
	 * 将传日的日期转换成指定日期格式的日期字符串
	 * @param date 要转换日期
	 * @param sFormatStr 格式字符串
	 * @return
	 */
	public static String getDateByFormat(Date date,String sFormatStr){
		if(date==null) return null;
		if(isNullOrEmpty(sFormatStr)) sFormatStr = SHORT_DATE_FMT;
		SimpleDateFormat formatter = new SimpleDateFormat(sFormatStr);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 
	 * @Title: getCurrLongDate
	 * @Description: 取当前时间字符串
	 * @return
	 * String 
	 * @throws
	 */
	public static String getCurrLongDate()
	{
		return getDateByFormat(LONG_DATE_FMT);
	}

	/**
	 * 根据当前登陆人编号返回一个时间类型的编号用于唯一编号
	 * @param sUserId 当前登陆人编号
	 * @return
	 */
	public static String getUUID(String sUserId){
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String dateString = formatter.format(currentTime);
		return sUserId+dateString;
	}

	/*
	 * 返回一个编号用于唯一编号
	 */
	public static String getUUID(){
		return java.util.UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * double直接输出时,如果小数点后位数太长会出现字母e,采用本方法转换为string再输出
	 * @param d
	 * @param 保留小数个数
	 * @return
	 * createuser:wyb
	 * createdate:2012-05-17
	 */
	public static String double2String(double d, int fNumber) {
		if (fNumber < 0) fNumber= 0;
		String pattern = null;
		switch (fNumber) {
		case 0:
			pattern = "#0";
			break;
		default:
			pattern = "#0."; 
			StringBuffer b = new StringBuffer(pattern);
			for (int i = 0; i < fNumber; i++) {
				b.append('#');
			}
			pattern = b.toString();
			break;
		}
		DecimalFormat formatter = new DecimalFormat();
		formatter.applyPattern(pattern);
		String value = formatter.format(d);
		return value;
	}

	/**
	 * 格式化double类型的字符串 主要是解决double过长会出现含有E的状况 如果传入的字符串不为double类型则返回 defaultValue
	 * @param d
	 * @param 保留小数位个数保留小数个数
	 * @param defaultValue 如果传入的值为null或“” 将其变为默认值
	 * @return
	 * createuser:wyb
	 * createdate:2012-05-17
	 */
	public static String formattingDoubleString(String d, int fNumber,double defaultValue) {
		return double2String(parseDouble(d,defaultValue),fNumber);
	}
	/**
	 * 将传的数据转换成字符串 如果 为空 或传入的数据不是整型数据则 返回 传入的 i
	 * @param s 待转换字符串
	 * @param i 字符串为空或非整形时返回的值
	 * @return
	 */
	public static int parseInt(String s,int i){
		if (isNullOrEmpty(s))
		{
			return i;
		}
		else
		{
			try{
				return Integer.parseInt(s);
			}catch(Exception e){
				return i;
			}
		}
	}
	public static int parseInt(String s)
	{
		if (isNullOrEmpty(s))
		{
			return 0;
		}
		else
		{
			return Integer.parseInt(s);
		}
	}
	public static double parseDouble(String s)
	{
		if (isNullOrEmpty(s))
		{
			return 0;
		}
		else
		{
			return Double.parseDouble(s);
		}
	}
	/**
	 * 将传的数据转换成double 如果 为空 或传入的数据不是double型数据则 返回 传入的d
	 * @param s
	 * @param d 字符串为空或浮点型时返回的值
	 * @return
	 */
	public static double parseDouble(String s,double d)
	{
		if (isNullOrEmpty(s))
		{
			return 0;
		}
		else
		{
			try{
				return Double.parseDouble(s);
			}catch(Exception e){
				return d;
			}
		}
	}
	/**
	 * 判断开始日期是否大于截止日期
	 * 
	 * @param sStartDate
	 *            开始日期
	 * @param sEndDate
	 *            截止日期
	 * @param dataFormat
	 *            日期格式
	 * @return
	 */
	public static boolean isDateQualified(String sStartDate, String sEndDate,
			String dataFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dataFormat);
		if (sStartDate != null && !StringUtil.isNullOrEmpty(sStartDate)
				&& sEndDate != null && !StringUtil.isNullOrEmpty(sEndDate)) {
			try {
				/*
				 * 将读到的日期进行比较 开始日期若小于截止日期，则返回true （转换成毫秒数，计算出多少天来进行判断）
				 */
				if ((sdf.parse(sStartDate).getTime() - sdf.parse(sEndDate)
						.getTime()) <= 0) {
					return true;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				//				e.printStackTrace();
				return false;
			}
			return false;
		}
		return false;
	}
	
	/**
	 * 字符数组转换为字符串
	 * @param values
	 * @return
	 */
	public static String aryToString(String[] values) {
		if(values == null || values.length == 0){
			return null;
		}
		StringBuffer sb = new StringBuffer(1024);
		for (int i = 0; i < values.length; i++ ) {
			sb.append(values[i]);
			if(i != values.length-1){
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param tranCode 交易吗
	 * @param userName 用户名
	 * @param msg 用户信息
	 * @return
	 */
	public static String string2FormatMsg(String tranCode, String msg, String userName){
		if(StringUtil.isNullOrEmpty(tranCode)||StringUtil.isNullOrEmpty(msg)){
			return null;
		}
		String str = StringUtil.isNullOrEmpty(userName)?"":("用户【"+userName+"】"); 
		return "【"+tranCode+"】"+str+msg;
	}
	
	/**
	 * 生成流水号：规则：终端号+当前时间+4位随机数字
	 * 
	 * @param tmnnum
	 * @return
	 */
	public static String getkeep(String tmnNum) {
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String time = df.format(new Date());
		Long random = (long) (Math.random() * Math.pow(10, 4));
		return tmnNum + time + random.toString();
	}
}
