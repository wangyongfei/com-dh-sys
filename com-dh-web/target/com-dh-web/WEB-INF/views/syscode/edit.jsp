<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
<script type="text/javascript">
    function save(){
        $.ajax({
            type:"POST",
            url:"save",
            data:$("#editForm").serialize(),
            dataType:"json",
            success:function(data){
                $.messager.alert("操作提示", data.msg, "info", function () {
                    parent.$('#myWindow').window('close');
                    parent.$('#list_data').datagrid('load',{});
                });
            },
            error:function(e){

                $.messager.alert("操作提示", "操作失败");
            }
        });
    }

    function cancel(){
        parent.$('#myWindow').window('close');
    }
</script>
<div style="height: 91%; margin: 0px; text-align: center; padding-top: 20px;">
    <form id="editForm" method="post">
        <input type="hidden" name="id" value="${vo.id}"/>
        <table align="center" style="font-size: 12px; padding: 0px;">
            <tr>
                <td>
                    <label>字典类型</label>
                </td>
                <td>
                    <input type="text" name="codeType" id="codeType"  value="${vo.codeType}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>字典码</label>
                </td>
                <td>
                    <input type="text" name="code" id="code"  value="${vo.code}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>字典名称</label>
                </td>
                <td>
                    <input type="text" name="codeName" id="codeName"  value="${vo.codeName}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>字典描述</label>
                </td>
                <td>
                    <input type="text" name="codeDec" id="codeDec"  value="${vo.codeDec}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>字典备注</label>
                </td>
                <td>
                    <input type="text" name="mark" id="mark"  value="${vo.mark}"/>
                </td>
            </tr>
                        <tr>
                <td>
                    <label>启用状态</label>
                </td>
                <td>
                    <input type="text" name="flag" id="flag"  value="${vo.flag}"/>
                </td>
            </tr>
                    </table>
    </form>
</div>
<div id="dlg-buttons" style="text-align: right; padding-right: 20px;">
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()" iconcls="icon-save">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="cancel()" iconCls="icon-cancel">取消</a>
</div>
</html>