<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
    <%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style type="text/css">
        body{
            padding: 0px;
            margin: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            $('#list_data').datagrid({
                title:"查询列表",
                class:"easyui-datagrid",
                url:"search",
                toolbar:"#toolbar",
                pagination:"true",
                rownumbers:"true",
                singleSelect:"false"
            });
            $("#list_data").datagrid('hideColumn', "id");
        });

        function _add(){
            showMyWindow(
                    "myWindow",
                    "编辑",
                    'edit',
                    1000,
                    600);
        }

        function _delete(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                $.messager.confirm("操作提示", "您确定要执行操作吗？", function (data) {
                    if (data) {
                        $.ajax({
                            type:"POST",
                            url:"delete",
                            data:{"id":row.id},
                            dataType:"json",
                            success:function(data){
                                $.messager.alert("操作提示", data.msg, "info", function () {
                                    $('#list_data').datagrid('load',{});
                                });
                            },
                            error:function(e){
                                $.messager.alert("操作提示", "操作失败");
                            }
                        });
                    }
                });

            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }

        function _update(){
            var row = $('#list_data').datagrid('getSelected');
            if (row){
                showMyWindow(
                        "myWindow",
                        "编辑",
                        'edit?id='+row.id);
            }else{
                $.messager.alert("操作提示", "请选择一条数据");
            }
        }
        function searchButton(){
            $('#list_data').datagrid('load',{
//                itemid: $('#itemid').val(),
//                productid: $('#productid').val()
            });
        }

        function formatStatus(val,row){
            if (val==0){
                return '<a href="#" class="easyui-linkbutton" style="color:red;">禁用</a>';

            } else {
                // return '<input type="button"  value=""/>';
                return '<a href="#" class="easyui-linkbutton" >启用</a>';

            }
        }

    </script>
</head>
<body style="padding: 0px;">
<div id="toolbar" style="padding:5px;height:auto">
    <div>
        日期: <input class="easyui-datebox" style="width:80px">至: <input class="easyui-datebox" style="width:80px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="searchButton();">查询</a>
    </div>
    <div style="margin-bottom:5px">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="_add();" style="color: black;">新增</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true"  onclick="_update();" style="color: black;">修改</a>
        <%--<a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true"></a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true"></a>--%>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="_delete();" style="color: black;">删除</a>
    </div>
</div>
<table id="list_data">
    <thead>
    <tr>
                <th field="id" width="120" style="display: none">主键</th>
                <th field="ck" checkbox="true"></th>
                <th field="codeType" width="120">字典类型</th>
                <th field="code" width="120">字典码</th>
                <th field="codeName" width="120">字典名称</th>
                <th field="codeDec" width="120">字典描述</th>
                <th field="mark" width="120" editor="{type:'text'}">字典备注</th>
                <th field="flag" width="120"  formatter="formatStatus">操作</th>
            </tr>
    </thead>
</table>
<div id="myWindow" class="easyui-dialog" closed="true"></div>
<%--<div class="easyui-layout" style="width:400px;height:200px;">--%>
    <%--<div region="west" split="true" title="Navigator" style="width:150px;">--%>
        <%--<p style="padding:5px;margin:0;">Select language:</p>--%>
        <%--<ul>--%>
            <%--<li><a href="javascript:void(0)" onclick="showcontent('java')">Java</a></li>--%>
            <%--<li><a href="javascript:void(0)" onclick="showcontent('cshape')">C#</a></li>--%>
            <%--<li><a href="javascript:void(0)" onclick="showcontent('vb')">VB</a></li>--%>
            <%--<li><a href="javascript:void(0)" onclick="showcontent('erlang')">Erlang</a></li>--%>
        <%--</ul>--%>
    <%--</div>--%>
    <%--<div id="content" region="center" title="Language" style="padding:5px;">--%>
    <%--</div>--%>
<%--</div>--%>
</body>
</html>