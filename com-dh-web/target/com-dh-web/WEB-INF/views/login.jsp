<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/WEB-INF/views/common/layout/meta.jsp" %>
<%@include file="/WEB-INF/views/common/layout/links.jsp" %>
    <style>
        html,body{
            color: #333;
        }
    </style>
    <script type="text/javascript">
       $(function(){
           var $win;
           $win = $("#win").window({
               title:"鼎航管理系统",
               width:400,
               height:260,
               shadow:false,
               closable:false,
               modal:true,
               collapsible:false,
               minimizable:false,
               maximizable:false,
               draggable:false,
               resizable:false
           });
           $win.window('open');
       });
        function login(){
            var username = $("#username").val();
            var password = $("#password").val();
            if(!username){
                $.messager.alert("操作提示", "用户名不能为空");
                return;
            }
            if(!password){
                $.messager.alert("操作提示", "密码不能为空");
                return;
            }


            ajaxLoading();
            $.ajax({
                type:"POST",
                url:"userLogin",
                data:$("#loginForm").serialize(),
                dataType:"json",

            success: function(data){
                    ajaxLoadClose();
                    if (data.success=="0") {
                        $.messager.alert("操作提示", "登陆成功", "info", function () {
                            window.location.href = "mainPage";
                        });
                    }else{
                        $.messager.alert("操作提示", "用户名或密码错误");
                    }
                },
                error: function (e) {
                    ajaxLoadClose();
                    $.messager.alert("操作提示", "登陆异常");
                }
            });
        }


    </script>
</head>
<body>
<div id="win" class="easyui-window"  closed="true">
    <form id="loginForm" style="padding:0px 0px 10px 0px;">
        <div style="text-align: center; border-bottom:solid 3px #817865; " class="easyui-layout">
        <img src="images/login/first_bg.jpg" alt="" width="400" height="55">
        </div>
        <table align="center" style="margin-top: 20px;">
            <tr><td>用户名:</td><td> <input name="username"  id="username" type="text" placeholder="用户名"/></td></tr>
            <tr><td>密  码:</td><td><input type="password"   name="password" id="password" placeholder="密码"></td></tr>
            </table>
        <div style="padding:5px;  text-align:center;margin-top: 20px;">
            <a href="#" onclick="login()" class="easyui-linkbutton" style="width: 70px;" icon="icon-ok">登陆</a><input type="checkbox"/>记住密码
        </div>
        </form>
    </form>
</div>
</body>
</html>