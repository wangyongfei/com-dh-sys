package com.dh.sys.sync;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/31.
 */
public class MongosyncEntity implements Serializable {

    private String id;

    private String clooectionName;

    private Date credateDate;


    @Override
    public String toString() {
        return "MongosyncEntity{" +
                "id='" + id + '\'' +
                ", clooectionName='" + clooectionName + '\'' +
                ", credateDate=" + credateDate +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClooectionName() {
        return clooectionName;
    }

    public void setClooectionName(String clooectionName) {
        this.clooectionName = clooectionName;
    }

    public Date getCredateDate() {
        return credateDate;
    }

    public void setCredateDate(Date credateDate) {
        this.credateDate = credateDate;
    }
}
