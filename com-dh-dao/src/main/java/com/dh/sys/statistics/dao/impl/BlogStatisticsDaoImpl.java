package com.dh.sys.statistics.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.statistics.dao.BlogStatisticsDao;
import com.dh.sys.statistics.model.BlogStatistics;
import com.dh.sys.statistics.model.BlogStatisticsCriteria;

@Repository("BlogStatisticsDao")
public class BlogStatisticsDaoImpl extends BaseDao implements BlogStatisticsDao {

    public BlogStatisticsDaoImpl() {
        super();
    }

    public int countByExample(BlogStatisticsCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("blog_statistics.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(BlogStatisticsCriteria example) {
        int rows = getSqlMapClientTemplate().delete("blog_statistics.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    BlogStatistics key = new BlogStatistics();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("blog_statistics.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(BlogStatistics record) {
        getSqlMapClientTemplate().insert("blog_statistics.ibatorgenerated_insert", record);
    }

    public void insertSelective(BlogStatistics record) {
        getSqlMapClientTemplate().insert("blog_statistics.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<BlogStatistics> selectByExample(BlogStatisticsCriteria example,Integer page, Integer rows) {
        List<BlogStatistics> list = getSqlMapClientTemplate().queryForList("blog_statistics.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<BlogStatistics> selectByExample(BlogStatisticsCriteria example) {
        List<BlogStatistics> list = getSqlMapClientTemplate().queryForList("blog_statistics.ibatorgenerated_selectByExample", example);
        return list;
    }

    public BlogStatistics selectByPrimaryKey(String id) {
        BlogStatistics key = new BlogStatistics();
        key.setId(id);
        BlogStatistics record = (BlogStatistics) getSqlMapClientTemplate().queryForObject("blog_statistics.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(BlogStatistics record, BlogStatisticsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("blog_statistics.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(BlogStatistics record, BlogStatisticsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("blog_statistics.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(BlogStatistics record) {
        int rows = getSqlMapClientTemplate().update("blog_statistics.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(BlogStatistics record) {
        int rows = getSqlMapClientTemplate().update("blog_statistics.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends BlogStatisticsCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, BlogStatisticsCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}