package com.dh.sys.statistics.model;

import java.io.Serializable;

public class BlogStatistics implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private String id;


    /**
    * 专栏编码
    */
    private String blogCode;


    /**
    * 时间戳
    */
    private String timeStamp;


    /**
    * 阅读用户
    */
    private String userCode;


    /**
    * 操作类型
    */
    private Integer operationType;


    /**
    * 专栏类型
    */
    private Integer blogType;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getBlogCode() {
        return blogCode;
    }

    public void setBlogCode(String blogCode) {
        this.blogCode = blogCode;
    }
    
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    
    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }
    
    public Integer getBlogType() {
        return blogType;
    }

    public void setBlogType(Integer blogType) {
        this.blogType = blogType;
    }
    

}