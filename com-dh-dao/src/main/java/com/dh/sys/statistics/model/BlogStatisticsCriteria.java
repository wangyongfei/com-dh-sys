package com.dh.sys.statistics.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class BlogStatisticsCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public BlogStatisticsCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected BlogStatisticsCriteria(BlogStatisticsCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("id is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("id is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("id =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("id <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("id >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("id >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("id <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("id <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("id like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("id not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("id in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("id not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("id between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("id not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andBlogCodeIsNull() {
        addCriterion("blog_code is null");
        return this;
    }

    public Criteria andBlogCodeIsNotNull() {
        addCriterion("blog_code is not null");
        return this;
    }

    public Criteria andBlogCodeEqualTo(String value) {
        addCriterion("blog_code =", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeNotEqualTo(String value) {
        addCriterion("blog_code <>", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeGreaterThan(String value) {
        addCriterion("blog_code >", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeGreaterThanOrEqualTo(String value) {
        addCriterion("blog_code >=", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeLessThan(String value) {
        addCriterion("blog_code <", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeLessThanOrEqualTo(String value) {
        addCriterion("blog_code <=", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeLike(String value) {
        addCriterion("blog_code like", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeNotLike(String value) {
        addCriterion("blog_code not like", value, "blogCode");
        return this;
    }

    public Criteria andBlogCodeIn(List<String> values) {
        addCriterion("blog_code in", values, "blogCode");
        return this;
    }

    public Criteria andBlogCodeNotIn(List<String> values) {
        addCriterion("blog_code not in", values, "blogCode");
        return this;
    }

    public Criteria andBlogCodeBetween(String value1, String value2) {
        addCriterion("blog_code between", value1, value2, "blogCode");
        return this;
    }

    public Criteria andBlogCodeNotBetween(String value1, String value2) {
        addCriterion("blog_code not between", value1, value2, "blogCode");
        return this;
    }

    
    public Criteria andTimeStampIsNull() {
        addCriterion("time_stamp is null");
        return this;
    }

    public Criteria andTimeStampIsNotNull() {
        addCriterion("time_stamp is not null");
        return this;
    }

    public Criteria andTimeStampEqualTo(String value) {
        addCriterion("time_stamp =", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampNotEqualTo(String value) {
        addCriterion("time_stamp <>", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampGreaterThan(String value) {
        addCriterion("time_stamp >", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampGreaterThanOrEqualTo(String value) {
        addCriterion("time_stamp >=", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampLessThan(String value) {
        addCriterion("time_stamp <", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampLessThanOrEqualTo(String value) {
        addCriterion("time_stamp <=", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampLike(String value) {
        addCriterion("time_stamp like", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampNotLike(String value) {
        addCriterion("time_stamp not like", value, "timeStamp");
        return this;
    }

    public Criteria andTimeStampIn(List<String> values) {
        addCriterion("time_stamp in", values, "timeStamp");
        return this;
    }

    public Criteria andTimeStampNotIn(List<String> values) {
        addCriterion("time_stamp not in", values, "timeStamp");
        return this;
    }

    public Criteria andTimeStampBetween(String value1, String value2) {
        addCriterion("time_stamp between", value1, value2, "timeStamp");
        return this;
    }

    public Criteria andTimeStampNotBetween(String value1, String value2) {
        addCriterion("time_stamp not between", value1, value2, "timeStamp");
        return this;
    }

    
    public Criteria andUserCodeIsNull() {
        addCriterion("user_code is null");
        return this;
    }

    public Criteria andUserCodeIsNotNull() {
        addCriterion("user_code is not null");
        return this;
    }

    public Criteria andUserCodeEqualTo(String value) {
        addCriterion("user_code =", value, "userCode");
        return this;
    }

    public Criteria andUserCodeNotEqualTo(String value) {
        addCriterion("user_code <>", value, "userCode");
        return this;
    }

    public Criteria andUserCodeGreaterThan(String value) {
        addCriterion("user_code >", value, "userCode");
        return this;
    }

    public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
        addCriterion("user_code >=", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLessThan(String value) {
        addCriterion("user_code <", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLessThanOrEqualTo(String value) {
        addCriterion("user_code <=", value, "userCode");
        return this;
    }

    public Criteria andUserCodeLike(String value) {
        addCriterion("user_code like", value, "userCode");
        return this;
    }

    public Criteria andUserCodeNotLike(String value) {
        addCriterion("user_code not like", value, "userCode");
        return this;
    }

    public Criteria andUserCodeIn(List<String> values) {
        addCriterion("user_code in", values, "userCode");
        return this;
    }

    public Criteria andUserCodeNotIn(List<String> values) {
        addCriterion("user_code not in", values, "userCode");
        return this;
    }

    public Criteria andUserCodeBetween(String value1, String value2) {
        addCriterion("user_code between", value1, value2, "userCode");
        return this;
    }

    public Criteria andUserCodeNotBetween(String value1, String value2) {
        addCriterion("user_code not between", value1, value2, "userCode");
        return this;
    }

    
    public Criteria andOperationTypeIsNull() {
        addCriterion("operation_type is null");
        return this;
    }

    public Criteria andOperationTypeIsNotNull() {
        addCriterion("operation_type is not null");
        return this;
    }

    public Criteria andOperationTypeEqualTo(Integer value) {
        addCriterion("operation_type =", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeNotEqualTo(Integer value) {
        addCriterion("operation_type <>", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeGreaterThan(Integer value) {
        addCriterion("operation_type >", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeGreaterThanOrEqualTo(Integer value) {
        addCriterion("operation_type >=", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeLessThan(Integer value) {
        addCriterion("operation_type <", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeLessThanOrEqualTo(Integer value) {
        addCriterion("operation_type <=", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeLike(Integer value) {
        addCriterion("operation_type like", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeNotLike(Integer value) {
        addCriterion("operation_type not like", value, "operationType");
        return this;
    }

    public Criteria andOperationTypeIn(List<Integer> values) {
        addCriterion("operation_type in", values, "operationType");
        return this;
    }

    public Criteria andOperationTypeNotIn(List<Integer> values) {
        addCriterion("operation_type not in", values, "operationType");
        return this;
    }

    public Criteria andOperationTypeBetween(Integer value1, Integer value2) {
        addCriterion("operation_type between", value1, value2, "operationType");
        return this;
    }

    public Criteria andOperationTypeNotBetween(Integer value1, String value2) {
        addCriterion("operation_type not between", value1, value2, "operationType");
        return this;
    }

    
    public Criteria andBlogTypeIsNull() {
        addCriterion("blog_type is null");
        return this;
    }

    public Criteria andBlogTypeIsNotNull() {
        addCriterion("blog_type is not null");
        return this;
    }

    public Criteria andBlogTypeEqualTo(Integer value) {
        addCriterion("blog_type =", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeNotEqualTo(Integer value) {
        addCriterion("blog_type <>", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeGreaterThan(Integer value) {
        addCriterion("blog_type >", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeGreaterThanOrEqualTo(Integer value) {
        addCriterion("blog_type >=", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeLessThan(Integer value) {
        addCriterion("blog_type <", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeLessThanOrEqualTo(Integer value) {
        addCriterion("blog_type <=", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeLike(Integer value) {
        addCriterion("blog_type like", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeNotLike(Integer value) {
        addCriterion("blog_type not like", value, "blogType");
        return this;
    }

    public Criteria andBlogTypeIn(List<Integer> values) {
        addCriterion("blog_type in", values, "blogType");
        return this;
    }

    public Criteria andBlogTypeNotIn(List<Integer> values) {
        addCriterion("blog_type not in", values, "blogType");
        return this;
    }

    public Criteria andBlogTypeBetween(Integer value1, Integer value2) {
        addCriterion("blog_type between", value1, value2, "blogType");
        return this;
    }

    public Criteria andBlogTypeNotBetween(Integer value1, String value2) {
        addCriterion("blog_type not between", value1, value2, "blogType");
        return this;
    }

        }
}