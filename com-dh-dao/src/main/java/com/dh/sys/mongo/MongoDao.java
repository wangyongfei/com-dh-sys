package com.dh.sys.mongo;


import com.dh.sys.sync.MongosyncEntity;
import com.dh.sys.syslog.model.SysLog;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.List;

import static org.springframework.data.domain.Sort.Direction;
import static org.springframework.data.domain.Sort.Order;

/**
 * mongodb操作底层
 * Created by Administrator on 2016/8/20.
 */
@Repository
public class MongoDao {

    @Autowired
    @Qualifier("mongoTemplate")
    private MongoTemplate mongoTemplate;

    @Autowired
    @Qualifier("mongoFSTemplate")
    private GridFsTemplate mongoFSTemplate;


    public List<FilePicsEntity> findList(int skip, int limit) {
        Query query = new Query();
        query.with(new Sort(new Order(Direction.ASC, "_id")));
        query.skip(skip).limit(limit);
        return this.mongoTemplate.find(query, FilePicsEntity.class);
    }
    public List<FilePicsEntity> findAllList() {
        Query query = new Query();
        query.with(new Sort(new Order(Direction.ASC, "_id")));
        return this.mongoTemplate.find(query, FilePicsEntity.class);
    }

    public void remove(String id) {
        Query query = new Query();
        query.addCriteria(new Criteria("id").is(id));
        this.mongoTemplate.remove(query, FilePicsEntity.class);
    }

    public FilePicsEntity findById(String id) {
        return this.mongoTemplate.findById(id, FilePicsEntity.class);
    }

    public void save(Object filePicsEntity) {
         this.mongoTemplate.save(filePicsEntity);
    }


    public GridFSFile store(File file, String fileName) { return store(file, fileName, ""); }

    public GridFSFile store(File file, String fileName, String contentType)
    {
          store(file, fileName, contentType, null);
           return null;
    }

    public void store(File file, String fileName, String contentType, DBObject metadata) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
             store(is, fileName, "", metadata);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
//            return null;
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public GridFSFile store(InputStream inputStream, String fileName) {
         store(inputStream, fileName, "");
         return null;
    }

    public GridFSFile store(InputStream content, String fileName, String contentType) {
         store(content, fileName, contentType, null);
         return null;
    }
//
    public GridFSFile store(InputStream content, String fileName, DBObject metadata) {
         store(content, fileName, "", null);
         return null;
    }

    public void  store(InputStream content, String fileName, String contentType, DBObject metadata) {
        synchronized (fileName) {

            mongoFSTemplate.store(content,fileName,contentType,metadata);

        }
    }

    public void removeFile(String fileName) {
        Criteria criteria = Criteria.where("fileName").is(fileName);
        Query query = new Query();
        query.addCriteria(criteria);
        GridFSDBFile gridfile =  mongoFSTemplate.findOne(query);
        if (gridfile == null) {
//      logger.error("File {} not exist!", new Object[] { fileName });
            return;
        }
        mongoFSTemplate.delete(query);
    }

    /**
     * mongo批量插入最大数据支持48M
     * @param list
     * @param collectionName collection  字符串
     */
    public void batchInsertByCollectionName(List<?>list,String collectionName){
        mongoTemplate.insert(list,collectionName);
    }


    /**
     *  mongo批量插入最大数据支持48M
     * @param list
     * @param clazz collection 类
     */
    public void batchInsertByCollectionClass(List<?>list,Object clazz){
        mongoTemplate.insert(list,clazz.getClass());
    }

    /**
     * collection 字符串
     * 创建collection
     * @param collectionName
     * @return
     */
    public DBCollection createCollectionName(String collectionName){
        return mongoTemplate.createCollection(collectionName);
    }

    /**
     * collection 类
     * 创建collection
     * @return
     */
    public DBCollection createCollectionClass(){
        return mongoTemplate.createCollection(MongosyncEntity.class);
    }

    public List<?> findAllList(String clooectionName) {
        Criteria criteria = Criteria.where("clooectionName").is(clooectionName);
        Query query = new Query();
        query.addCriteria(criteria);
        return mongoTemplate.find(query, MongosyncEntity.class);
    }

    public void findAllList(SysLog sysLog1, String collectionName) {
//        Criteria criteria = Criteria.where("clooectionName").is(clooectionName);
//        Query query = new Query();
//        query.addCriteria(criteria);
        //mongoTemplate.findAll(query,collectionName);
    }
}
