package com.dh.sys.sysconfigcenter.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.sysconfigcenter.dao.SysConfigCenterDao;
import com.dh.sys.sysconfigcenter.model.SysConfigCenter;
import com.dh.sys.sysconfigcenter.model.SysConfigCenterCriteria;

@Repository("SysConfigCenterDao")
public class SysConfigCenterDaoImpl extends BaseDao implements SysConfigCenterDao {

    public SysConfigCenterDaoImpl() {
        super();
    }

    public int countByExample(SysConfigCenterCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("sys_config_center.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(SysConfigCenterCriteria example) {
        int rows = getSqlMapClientTemplate().delete("sys_config_center.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    SysConfigCenter key = new SysConfigCenter();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("sys_config_center.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(SysConfigCenter record) {
        getSqlMapClientTemplate().insert("sys_config_center.ibatorgenerated_insert", record);
    }

    public void insertSelective(SysConfigCenter record) {
        getSqlMapClientTemplate().insert("sys_config_center.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<SysConfigCenter> selectByExample(SysConfigCenterCriteria example,Integer page, Integer rows) {
        List<SysConfigCenter> list = getSqlMapClientTemplate().queryForList("sys_config_center.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<SysConfigCenter> selectByExample(SysConfigCenterCriteria example) {
        List<SysConfigCenter> list = getSqlMapClientTemplate().queryForList("sys_config_center.ibatorgenerated_selectByExample", example);
        return list;
    }

    public SysConfigCenter selectByPrimaryKey(String id) {
        SysConfigCenter key = new SysConfigCenter();
        key.setId(id);
        SysConfigCenter record = (SysConfigCenter) getSqlMapClientTemplate().queryForObject("sys_config_center.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(SysConfigCenter record, SysConfigCenterCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_config_center.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(SysConfigCenter record, SysConfigCenterCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("sys_config_center.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(SysConfigCenter record) {
        int rows = getSqlMapClientTemplate().update("sys_config_center.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(SysConfigCenter record) {
        int rows = getSqlMapClientTemplate().update("sys_config_center.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends SysConfigCenterCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, SysConfigCenterCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}