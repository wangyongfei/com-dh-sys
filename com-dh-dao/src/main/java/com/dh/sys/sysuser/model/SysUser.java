package com.dh.sys.sysuser.model;

import java.io.Serializable;

public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 用户编号
    */
    private String id;


    /**
    * 用户编号
    */
    private String userName;


    /**
    * 
    */
    private String password;


    /**
    * 
    */
    private String createDate;


    /**
    * 是否启用 0：未启用 1：启用
    */
    private Integer flag;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    

}