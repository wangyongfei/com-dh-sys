package com.dh.sys.sysuser.dao;

import com.dh.sys.sysuser.model.SysUser;
import com.dh.sys.sysuser.model.SysUserCriteria;
import java.util.List;

public interface SysUserDao {

    int countByExample(SysUserCriteria example);


    int deleteByExample(SysUserCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysUser record);


    void insertSelective(SysUser record);


    List<SysUser> selectByExample(SysUserCriteria example,Integer page, Integer rows);


    SysUser selectByPrimaryKey(String id);


    int updateByExampleSelective(SysUser record, SysUserCriteria example);


    int updateByExample(SysUser record, SysUserCriteria example);


    int updateByPrimaryKeySelective(SysUser record);


    int updateByPrimaryKey(SysUser record);

    List<SysUser> selectByExample(SysUserCriteria example);

}