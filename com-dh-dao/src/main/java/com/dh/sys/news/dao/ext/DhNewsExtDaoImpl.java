package com.dh.sys.news.dao.ext;

import com.dh.sys.common.BaseDao;
import com.dh.sys.news.model.DhNews;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2016/9/17.
 */

@Transactional
@Repository("dhNewsExtDaoImpl")
public class DhNewsExtDaoImpl extends BaseDao{
    public DhNewsExtDaoImpl() {
        super();
    }

    public  String getMaxCodeNews(){
        DhNews key = new DhNews();
        Object object = getSqlMapClientTemplate().queryForObject("dh_news_ext.getMaxCodeNews", key);
        if(null == object){
            return null;
        }
        return object.toString();
    }
}
