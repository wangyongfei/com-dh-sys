package com.dh.sys.news.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.news.dao.DhNewsDao;
import com.dh.sys.news.model.DhNews;
import com.dh.sys.news.model.DhNewsCriteria;

@Repository("DhNewsDao")
public class DhNewsDaoImpl extends BaseDao implements DhNewsDao {

    public DhNewsDaoImpl() {
        super();
    }

    public int countByExample(DhNewsCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("dh_news.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(DhNewsCriteria example) {
        int rows = getSqlMapClientTemplate().delete("dh_news.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    DhNews key = new DhNews();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("dh_news.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(DhNews record) {
        getSqlMapClientTemplate().insert("dh_news.ibatorgenerated_insert", record);
    }

    public void insertSelective(DhNews record) {
        getSqlMapClientTemplate().insert("dh_news.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExample(DhNewsCriteria example,Integer page, Integer rows) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExample(DhNewsCriteria example) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExample", example);
        return list;
    }

    public DhNews selectByPrimaryKey(String id) {
        DhNews key = new DhNews();
        key.setId(id);
        DhNews record = (DhNews) getSqlMapClientTemplate().queryForObject("dh_news.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(DhNews record, DhNewsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(DhNews record, DhNewsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(DhNews record) {
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(DhNews record) {
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExampleWithBLOBs(DhNewsCriteria example) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExampleWithBLOBs", example);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExampleWithBLOBs(DhNewsCriteria example,Integer page, Integer rows) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExampleWithBLOBs", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExampleWithoutBLOBs(DhNewsCriteria example) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExample", example);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<DhNews> selectByExampleWithoutBLOBs(DhNewsCriteria example,Integer page, Integer rows) {
        List<DhNews> list = getSqlMapClientTemplate().queryForList("dh_news.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    public int updateByExampleWithBLOBs(DhNews record, DhNewsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByExampleWithBLOBs", parms);
        return rows;
    }

    public int updateByExampleWithoutBLOBs(DhNews record, DhNewsCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeyWithBLOBs(DhNews record) {
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByPrimaryKeyWithBLOBs", record);
        return rows;
    }

    public int updateByPrimaryKeyWithoutBLOBs(DhNews record) {
        int rows = getSqlMapClientTemplate().update("dh_news.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }
     private static class UpdateByExampleParms extends DhNewsCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, DhNewsCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}