package com.dh.sys.news.dao;

import com.dh.sys.news.model.DhNews;
import com.dh.sys.news.model.DhNewsCriteria;
import java.util.List;

public interface DhNewsDao {

    int countByExample(DhNewsCriteria example);


    int deleteByExample(DhNewsCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(DhNews record);


    void insertSelective(DhNews record);


    List<DhNews> selectByExample(DhNewsCriteria example,Integer page, Integer rows);


    DhNews selectByPrimaryKey(String id);


    int updateByExampleSelective(DhNews record, DhNewsCriteria example);


    int updateByExample(DhNews record, DhNewsCriteria example);


    int updateByPrimaryKeySelective(DhNews record);


    int updateByPrimaryKey(DhNews record);

    List<DhNews> selectByExample(DhNewsCriteria example);

    List<DhNews> selectByExampleWithBLOBs(DhNewsCriteria example);

    List<DhNews> selectByExampleWithBLOBs(DhNewsCriteria example, Integer page, Integer rows);

    List<DhNews> selectByExampleWithoutBLOBs(DhNewsCriteria example);

    List<DhNews> selectByExampleWithoutBLOBs(DhNewsCriteria example, Integer page, Integer rows);

    int updateByExampleWithBLOBs(DhNews record, DhNewsCriteria example);

    int updateByExampleWithoutBLOBs(DhNews record, DhNewsCriteria example);

    int updateByPrimaryKeyWithBLOBs(DhNews record);

    int updateByPrimaryKeyWithoutBLOBs(DhNews record);

    
}