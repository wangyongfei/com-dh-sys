package com.dh.sys.syscode.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 数据库条件操作类
*  create by  Admin at $date.format('yyyy-MM-dd HH:mm:ss',$createDate)
*/
public class SysCodeCriteria {

    /**
    * 排序字段
    */
    protected String orderByClause;

    /**
    * 数据库字段拼接集合
    */
    protected List<Criteria> oredCriteria;

    /**
    * 实例化对象的同时，为oredCriteria实例化
    */
    public SysCodeCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
    * 1.实例化对象的同时，为oredCriteria实例化
    * 2.实例化对象的同时，为排序字段orderByClause赋值
    * @param example
    */
    protected SysCodeCriteria(SysCodeCriteria example) {
        this.orderByClause = example.orderByClause;
        this.oredCriteria = example.oredCriteria;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }


    public void or(Criteria criteria) {
     oredCriteria.add(criteria);
    }


    public Criteria createCriteria() {
        /**
        * 获取criteria实例化对象
        */
        Criteria criteria = createCriteriaInternal();

        /**
        * 首次调用时如果数据库字段拼接集合为空，
        * 则获取一个新的实例对象加到该结合中
        */
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
    * 获取一个Criteria实例对象
    * @return
    */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }


    /**
    * 清空数据库字段拼接集合
    */
    public void clear() {
        oredCriteria.clear();
    }


    /**
    * 数据库字段拼接实体类
    */
    public static class Criteria {
    protected List<String> criteriaWithoutValue;

    protected List<Map<String, Object>> criteriaWithSingleValue;

    protected List<Map<String, Object>> criteriaWithListValue;

    protected List<Map<String, Object>> criteriaWithBetweenValue;

    protected Criteria() {
        super();
        criteriaWithoutValue = new ArrayList<String>();
        criteriaWithSingleValue = new ArrayList<Map<String, Object>>();
        criteriaWithListValue = new ArrayList<Map<String, Object>>();
        criteriaWithBetweenValue = new ArrayList<Map<String, Object>>();
    }

    public boolean isValid() {
        return criteriaWithoutValue.size() > 0
        || criteriaWithSingleValue.size() > 0
        || criteriaWithListValue.size() > 0
        || criteriaWithBetweenValue.size() > 0;
    }

    public List<String> getCriteriaWithoutValue() {
        return criteriaWithoutValue;
    }

    public List<Map<String, Object>> getCriteriaWithSingleValue() {
        return criteriaWithSingleValue;
    }

    public List<Map<String, Object>> getCriteriaWithListValue() {
        return criteriaWithListValue;
    }

    public List<Map<String, Object>> getCriteriaWithBetweenValue() {
        return criteriaWithBetweenValue;
    }

    protected void addCriterion(String condition) {
        if (condition == null) {
            throw new RuntimeException("Value for condition cannot be null");
        }
        criteriaWithoutValue.add(condition);
    }

    protected void addCriterion(String condition, Object value, String property) {
        if (value == null) {
            throw new RuntimeException("Value for " + property + " cannot be null");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("value", value);
        criteriaWithSingleValue.add(map);
    }

    protected void addCriterion(String condition, List<? extends Object> values, String property) {
        if (values == null || values.size() == 0) {
            throw new RuntimeException("Value list for " + property + " cannot be null or empty");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", values);
        criteriaWithListValue.add(map);
    }

    protected void addCriterion(String condition, Object value1, Object value2, String property) {
        if (value1 == null || value2 == null) {
            throw new RuntimeException("Between values for " + property + " cannot be null");
        }
        List<Object> list = new ArrayList<Object>();
        list.add(value1);
        list.add(value2);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("condition", condition);
        map.put("values", list);
        criteriaWithBetweenValue.add(map);
    }


    
    public Criteria andIdIsNull() {
        addCriterion("id is null");
        return this;
    }

    public Criteria andIdIsNotNull() {
        addCriterion("id is not null");
        return this;
    }

    public Criteria andIdEqualTo(String value) {
        addCriterion("id =", value, "id");
        return this;
    }

    public Criteria andIdNotEqualTo(String value) {
        addCriterion("id <>", value, "id");
        return this;
    }

    public Criteria andIdGreaterThan(String value) {
        addCriterion("id >", value, "id");
        return this;
    }

    public Criteria andIdGreaterThanOrEqualTo(String value) {
        addCriterion("id >=", value, "id");
        return this;
    }

    public Criteria andIdLessThan(String value) {
        addCriterion("id <", value, "id");
        return this;
    }

    public Criteria andIdLessThanOrEqualTo(String value) {
        addCriterion("id <=", value, "id");
        return this;
    }

    public Criteria andIdLike(String value) {
        addCriterion("id like", value, "id");
        return this;
    }

    public Criteria andIdNotLike(String value) {
        addCriterion("id not like", value, "id");
        return this;
    }

    public Criteria andIdIn(List<String> values) {
        addCriterion("id in", values, "id");
        return this;
    }

    public Criteria andIdNotIn(List<String> values) {
        addCriterion("id not in", values, "id");
        return this;
    }

    public Criteria andIdBetween(String value1, String value2) {
        addCriterion("id between", value1, value2, "id");
        return this;
    }

    public Criteria andIdNotBetween(String value1, String value2) {
        addCriterion("id not between", value1, value2, "id");
        return this;
    }

    
    public Criteria andCodeTypeIsNull() {
        addCriterion("code_type is null");
        return this;
    }

    public Criteria andCodeTypeIsNotNull() {
        addCriterion("code_type is not null");
        return this;
    }

    public Criteria andCodeTypeEqualTo(String value) {
        addCriterion("code_type =", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeNotEqualTo(String value) {
        addCriterion("code_type <>", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeGreaterThan(String value) {
        addCriterion("code_type >", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeGreaterThanOrEqualTo(String value) {
        addCriterion("code_type >=", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeLessThan(String value) {
        addCriterion("code_type <", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeLessThanOrEqualTo(String value) {
        addCriterion("code_type <=", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeLike(String value) {
        addCriterion("code_type like", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeNotLike(String value) {
        addCriterion("code_type not like", value, "codeType");
        return this;
    }

    public Criteria andCodeTypeIn(List<String> values) {
        addCriterion("code_type in", values, "codeType");
        return this;
    }

    public Criteria andCodeTypeNotIn(List<String> values) {
        addCriterion("code_type not in", values, "codeType");
        return this;
    }

    public Criteria andCodeTypeBetween(String value1, String value2) {
        addCriterion("code_type between", value1, value2, "codeType");
        return this;
    }

    public Criteria andCodeTypeNotBetween(String value1, String value2) {
        addCriterion("code_type not between", value1, value2, "codeType");
        return this;
    }

    
    public Criteria andCodeIsNull() {
        addCriterion("code is null");
        return this;
    }

    public Criteria andCodeIsNotNull() {
        addCriterion("code is not null");
        return this;
    }

    public Criteria andCodeEqualTo(String value) {
        addCriterion("code =", value, "code");
        return this;
    }

    public Criteria andCodeNotEqualTo(String value) {
        addCriterion("code <>", value, "code");
        return this;
    }

    public Criteria andCodeGreaterThan(String value) {
        addCriterion("code >", value, "code");
        return this;
    }

    public Criteria andCodeGreaterThanOrEqualTo(String value) {
        addCriterion("code >=", value, "code");
        return this;
    }

    public Criteria andCodeLessThan(String value) {
        addCriterion("code <", value, "code");
        return this;
    }

    public Criteria andCodeLessThanOrEqualTo(String value) {
        addCriterion("code <=", value, "code");
        return this;
    }

    public Criteria andCodeLike(String value) {
        addCriterion("code like", value, "code");
        return this;
    }

    public Criteria andCodeNotLike(String value) {
        addCriterion("code not like", value, "code");
        return this;
    }

    public Criteria andCodeIn(List<String> values) {
        addCriterion("code in", values, "code");
        return this;
    }

    public Criteria andCodeNotIn(List<String> values) {
        addCriterion("code not in", values, "code");
        return this;
    }

    public Criteria andCodeBetween(String value1, String value2) {
        addCriterion("code between", value1, value2, "code");
        return this;
    }

    public Criteria andCodeNotBetween(String value1, String value2) {
        addCriterion("code not between", value1, value2, "code");
        return this;
    }

    
    public Criteria andCodeNameIsNull() {
        addCriterion("code_name is null");
        return this;
    }

    public Criteria andCodeNameIsNotNull() {
        addCriterion("code_name is not null");
        return this;
    }

    public Criteria andCodeNameEqualTo(String value) {
        addCriterion("code_name =", value, "codeName");
        return this;
    }

    public Criteria andCodeNameNotEqualTo(String value) {
        addCriterion("code_name <>", value, "codeName");
        return this;
    }

    public Criteria andCodeNameGreaterThan(String value) {
        addCriterion("code_name >", value, "codeName");
        return this;
    }

    public Criteria andCodeNameGreaterThanOrEqualTo(String value) {
        addCriterion("code_name >=", value, "codeName");
        return this;
    }

    public Criteria andCodeNameLessThan(String value) {
        addCriterion("code_name <", value, "codeName");
        return this;
    }

    public Criteria andCodeNameLessThanOrEqualTo(String value) {
        addCriterion("code_name <=", value, "codeName");
        return this;
    }

    public Criteria andCodeNameLike(String value) {
        addCriterion("code_name like", value, "codeName");
        return this;
    }

    public Criteria andCodeNameNotLike(String value) {
        addCriterion("code_name not like", value, "codeName");
        return this;
    }

    public Criteria andCodeNameIn(List<String> values) {
        addCriterion("code_name in", values, "codeName");
        return this;
    }

    public Criteria andCodeNameNotIn(List<String> values) {
        addCriterion("code_name not in", values, "codeName");
        return this;
    }

    public Criteria andCodeNameBetween(String value1, String value2) {
        addCriterion("code_name between", value1, value2, "codeName");
        return this;
    }

    public Criteria andCodeNameNotBetween(String value1, String value2) {
        addCriterion("code_name not between", value1, value2, "codeName");
        return this;
    }

    
    public Criteria andCodeDecIsNull() {
        addCriterion("code_dec is null");
        return this;
    }

    public Criteria andCodeDecIsNotNull() {
        addCriterion("code_dec is not null");
        return this;
    }

    public Criteria andCodeDecEqualTo(String value) {
        addCriterion("code_dec =", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecNotEqualTo(String value) {
        addCriterion("code_dec <>", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecGreaterThan(String value) {
        addCriterion("code_dec >", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecGreaterThanOrEqualTo(String value) {
        addCriterion("code_dec >=", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecLessThan(String value) {
        addCriterion("code_dec <", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecLessThanOrEqualTo(String value) {
        addCriterion("code_dec <=", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecLike(String value) {
        addCriterion("code_dec like", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecNotLike(String value) {
        addCriterion("code_dec not like", value, "codeDec");
        return this;
    }

    public Criteria andCodeDecIn(List<String> values) {
        addCriterion("code_dec in", values, "codeDec");
        return this;
    }

    public Criteria andCodeDecNotIn(List<String> values) {
        addCriterion("code_dec not in", values, "codeDec");
        return this;
    }

    public Criteria andCodeDecBetween(String value1, String value2) {
        addCriterion("code_dec between", value1, value2, "codeDec");
        return this;
    }

    public Criteria andCodeDecNotBetween(String value1, String value2) {
        addCriterion("code_dec not between", value1, value2, "codeDec");
        return this;
    }

    
    public Criteria andMarkIsNull() {
        addCriterion("mark is null");
        return this;
    }

    public Criteria andMarkIsNotNull() {
        addCriterion("mark is not null");
        return this;
    }

    public Criteria andMarkEqualTo(String value) {
        addCriterion("mark =", value, "mark");
        return this;
    }

    public Criteria andMarkNotEqualTo(String value) {
        addCriterion("mark <>", value, "mark");
        return this;
    }

    public Criteria andMarkGreaterThan(String value) {
        addCriterion("mark >", value, "mark");
        return this;
    }

    public Criteria andMarkGreaterThanOrEqualTo(String value) {
        addCriterion("mark >=", value, "mark");
        return this;
    }

    public Criteria andMarkLessThan(String value) {
        addCriterion("mark <", value, "mark");
        return this;
    }

    public Criteria andMarkLessThanOrEqualTo(String value) {
        addCriterion("mark <=", value, "mark");
        return this;
    }

    public Criteria andMarkLike(String value) {
        addCriterion("mark like", value, "mark");
        return this;
    }

    public Criteria andMarkNotLike(String value) {
        addCriterion("mark not like", value, "mark");
        return this;
    }

    public Criteria andMarkIn(List<String> values) {
        addCriterion("mark in", values, "mark");
        return this;
    }

    public Criteria andMarkNotIn(List<String> values) {
        addCriterion("mark not in", values, "mark");
        return this;
    }

    public Criteria andMarkBetween(String value1, String value2) {
        addCriterion("mark between", value1, value2, "mark");
        return this;
    }

    public Criteria andMarkNotBetween(String value1, String value2) {
        addCriterion("mark not between", value1, value2, "mark");
        return this;
    }

    
    public Criteria andFlagIsNull() {
        addCriterion("flag is null");
        return this;
    }

    public Criteria andFlagIsNotNull() {
        addCriterion("flag is not null");
        return this;
    }

    public Criteria andFlagEqualTo(Integer value) {
        addCriterion("flag =", value, "flag");
        return this;
    }

    public Criteria andFlagNotEqualTo(Integer value) {
        addCriterion("flag <>", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThan(Integer value) {
        addCriterion("flag >", value, "flag");
        return this;
    }

    public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
        addCriterion("flag >=", value, "flag");
        return this;
    }

    public Criteria andFlagLessThan(Integer value) {
        addCriterion("flag <", value, "flag");
        return this;
    }

    public Criteria andFlagLessThanOrEqualTo(Integer value) {
        addCriterion("flag <=", value, "flag");
        return this;
    }

    public Criteria andFlagLike(Integer value) {
        addCriterion("flag like", value, "flag");
        return this;
    }

    public Criteria andFlagNotLike(Integer value) {
        addCriterion("flag not like", value, "flag");
        return this;
    }

    public Criteria andFlagIn(List<Integer> values) {
        addCriterion("flag in", values, "flag");
        return this;
    }

    public Criteria andFlagNotIn(List<Integer> values) {
        addCriterion("flag not in", values, "flag");
        return this;
    }

    public Criteria andFlagBetween(Integer value1, Integer value2) {
        addCriterion("flag between", value1, value2, "flag");
        return this;
    }

    public Criteria andFlagNotBetween(Integer value1, String value2) {
        addCriterion("flag not between", value1, value2, "flag");
        return this;
    }

        }
}