package com.dh.sys.syscode.model;

import java.io.Serializable;

public class SysCode implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private String id;


    /**
    * 字典类型
    */
    private String codeType;


    /**
    * 字典码
    */
    private String code;


    /**
    * 字典名称
    */
    private String codeName;


    /**
    * 字典描述
    */
    private String codeDec;


    /**
    * 字典备注
    */
    private String mark;


    /**
    * 启用状态
    */
    private Integer flag;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }
    
    public String getCodeDec() {
        return codeDec;
    }

    public void setCodeDec(String codeDec) {
        this.codeDec = codeDec;
    }
    
    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
    
    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    

}