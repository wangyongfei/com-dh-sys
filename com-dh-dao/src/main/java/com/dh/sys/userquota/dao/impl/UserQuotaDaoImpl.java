package com.dh.sys.userquota.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.userquota.dao.UserQuotaDao;
import com.dh.sys.userquota.model.UserQuota;
import com.dh.sys.userquota.model.UserQuotaCriteria;

@Repository("UserQuotaDao")
public class UserQuotaDaoImpl extends BaseDao implements UserQuotaDao {

    public UserQuotaDaoImpl() {
        super();
    }

    public int countByExample(UserQuotaCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("user_quota.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(UserQuotaCriteria example) {
        int rows = getSqlMapClientTemplate().delete("user_quota.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(Long id) {
        UserQuota key = new UserQuota();
                key.setId(id);
        int rows = getSqlMapClientTemplate().delete("user_quota.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(UserQuota record) {
        getSqlMapClientTemplate().insert("user_quota.ibatorgenerated_insert", record);
    }

    public void insertSelective(UserQuota record) {
        getSqlMapClientTemplate().insert("user_quota.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<UserQuota> selectByExample(UserQuotaCriteria example,Integer page, Integer rows) {
        List<UserQuota> list = getSqlMapClientTemplate().queryForList("user_quota.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<UserQuota> selectByExample(UserQuotaCriteria example) {
        List<UserQuota> list = getSqlMapClientTemplate().queryForList("user_quota.ibatorgenerated_selectByExample", example);
        return list;
    }

    public UserQuota selectByPrimaryKey(Long id) {
        UserQuota key = new UserQuota();
                key.setId(id);
        UserQuota record = (UserQuota) getSqlMapClientTemplate().queryForObject("user_quota.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(UserQuota record, UserQuotaCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("user_quota.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(UserQuota record, UserQuotaCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("user_quota.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(UserQuota record) {
        int rows = getSqlMapClientTemplate().update("user_quota.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(UserQuota record) {
        int rows = getSqlMapClientTemplate().update("user_quota.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends UserQuotaCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, UserQuotaCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}