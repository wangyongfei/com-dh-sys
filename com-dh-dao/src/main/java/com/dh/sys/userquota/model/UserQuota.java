package com.dh.sys.userquota.model;

import java.io.Serializable;

public class UserQuota implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private Long id;


    /**
    * 用户编号
    */
    private String uid;


    /**
    * 可用额度
    */
    private Long creditQuota;


    /**
    * 已用额度
    */
    private Long usedQuota;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    
    public Long getCreditQuota() {
        return creditQuota;
    }

    public void setCreditQuota(Long creditQuota) {
        this.creditQuota = creditQuota;
    }
    
    public Long getUsedQuota() {
        return usedQuota;
    }

    public void setUsedQuota(Long usedQuota) {
        this.usedQuota = usedQuota;
    }
    

}