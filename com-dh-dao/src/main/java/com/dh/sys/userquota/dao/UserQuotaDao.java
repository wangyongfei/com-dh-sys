package com.dh.sys.userquota.dao;

import com.dh.sys.userquota.model.UserQuota;
import com.dh.sys.userquota.model.UserQuotaCriteria;
import java.util.List;

public interface UserQuotaDao {

    int countByExample(UserQuotaCriteria example);


    int deleteByExample(UserQuotaCriteria example);


    int deleteByPrimaryKey(Long id);


    void insert(UserQuota record);


    void insertSelective(UserQuota record);


    List<UserQuota> selectByExample(UserQuotaCriteria example,Integer page, Integer rows);


    UserQuota selectByPrimaryKey(Long id);


    int updateByExampleSelective(UserQuota record, UserQuotaCriteria example);


    int updateByExample(UserQuota record, UserQuotaCriteria example);


    int updateByPrimaryKeySelective(UserQuota record);


    int updateByPrimaryKey(UserQuota record);

    List<UserQuota> selectByExample(UserQuotaCriteria example);

    
}