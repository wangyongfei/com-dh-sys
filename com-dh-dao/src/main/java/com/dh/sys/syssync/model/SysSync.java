package com.dh.sys.syssync.model;

import java.io.Serializable;

public class SysSync implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private String id;


    /**
    * 进件号
    */
    private String loanId;


    /**
    * 流程实例
    */
    private String processinstanceid;


    /**
    * 创建时间
    */
    private String creteDate;


    /**
    * 操作时间
    */
    private String optDate;


    /**
    * 完成时间
    */
    private String finishDate;


    /**
    * 任务状态
    */
    private Integer status;


    /**
    * 调用次数
    */
    private Integer callCount;


    /**
    * 用户编号
    */
    private String uid;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }
    
    public String getProcessinstanceid() {
        return processinstanceid;
    }

    public void setProcessinstanceid(String processinstanceid) {
        this.processinstanceid = processinstanceid;
    }
    
    public String getCreteDate() {
        return creteDate;
    }

    public void setCreteDate(String creteDate) {
        this.creteDate = creteDate;
    }
    
    public String getOptDate() {
        return optDate;
    }

    public void setOptDate(String optDate) {
        this.optDate = optDate;
    }
    
    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public Integer getCallCount() {
        return callCount;
    }

    public void setCallCount(Integer callCount) {
        this.callCount = callCount;
    }
    
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    

}