package com.dh.sys.syssync.dao;

import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.model.SysSyncCriteria;
import java.util.List;

public interface SysSyncDao {

    int countByExample(SysSyncCriteria example);


    int deleteByExample(SysSyncCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysSync record);


    void insertSelective(SysSync record);


    List<SysSync> selectByExample(SysSyncCriteria example,Integer page, Integer rows);


    SysSync selectByPrimaryKey(String id);


    int updateByExampleSelective(SysSync record, SysSyncCriteria example);


    int updateByExample(SysSync record, SysSyncCriteria example);


    int updateByPrimaryKeySelective(SysSync record);


    int updateByPrimaryKey(SysSync record);

    List<SysSync> selectByExample(SysSyncCriteria example);

    
}