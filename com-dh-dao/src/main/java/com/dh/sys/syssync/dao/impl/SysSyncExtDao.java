package com.dh.sys.syssync.dao.impl;

import com.dh.sys.common.BaseDao;
import com.dh.sys.syssync.model.SysSync;
import com.dh.sys.syssync.model.SysSyncCriteria;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/7.
 */
@Repository("SysSyncExtDao")
public class SysSyncExtDao extends BaseDao {

    /**
     * 查询数据 - 排除正在执行的用户，保证不同的系统任务请求队列中不会包含重复用户的情况。
     * @return
     */
   // @Select({"select A.* from sys_sync A where A.status in (#{unStatus}) and A.uid not in((select B.uid from sys_sync B where B.status = #{status}))"})
    public List<SysSync> findSysSyncData(Map<String,Object> map){
        List<SysSync> list = getSqlMapClientTemplate().queryForList("sys_sync_ext.ibatorgenerated_selectByExample", map);
        return list;
    }
}
