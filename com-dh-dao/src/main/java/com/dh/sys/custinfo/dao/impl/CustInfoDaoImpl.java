package com.dh.sys.custinfo.dao.impl;

import java.util.List;
import com.dh.sys.common.BaseDao;
import org.springframework.stereotype.Repository;

import com.dh.sys.custinfo.dao.CustInfoDao;
import com.dh.sys.custinfo.model.CustInfo;
import com.dh.sys.custinfo.model.CustInfoCriteria;

@Repository("CustInfoDao")
public class CustInfoDaoImpl extends BaseDao implements CustInfoDao {

    public CustInfoDaoImpl() {
        super();
    }

    public int countByExample(CustInfoCriteria example) {
        Integer count = (Integer)  getSqlMapClientTemplate().queryForObject("cust_info.ibatorgenerated_countByExample", example);
        return count;
    }

    public int deleteByExample(CustInfoCriteria example) {
        int rows = getSqlMapClientTemplate().delete("cust_info.ibatorgenerated_deleteByExample", example);
        return rows;
    }

    public int deleteByPrimaryKey(String id) {
    CustInfo key = new CustInfo();
        key.setId(id);
        int rows = getSqlMapClientTemplate().delete("cust_info.ibatorgenerated_deleteByPrimaryKey", key);
        return rows;
    }

    public void insert(CustInfo record) {
        getSqlMapClientTemplate().insert("cust_info.ibatorgenerated_insert", record);
    }

    public void insertSelective(CustInfo record) {
        getSqlMapClientTemplate().insert("cust_info.ibatorgenerated_insertSelective", record);
    }

    @SuppressWarnings("unchecked")
    public List<CustInfo> selectByExample(CustInfoCriteria example,Integer page, Integer rows) {
        List<CustInfo> list = getSqlMapClientTemplate().queryForList("cust_info.ibatorgenerated_selectByExample", example,page, rows);
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<CustInfo> selectByExample(CustInfoCriteria example) {
        List<CustInfo> list = getSqlMapClientTemplate().queryForList("cust_info.ibatorgenerated_selectByExample", example);
        return list;
    }

    public CustInfo selectByPrimaryKey(String id) {
        CustInfo key = new CustInfo();
        key.setId(id);
        CustInfo record = (CustInfo) getSqlMapClientTemplate().queryForObject("cust_info.ibatorgenerated_selectByPrimaryKey", key);
        return record;
    }

    public int updateByExampleSelective(CustInfo record, CustInfoCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("cust_info.ibatorgenerated_updateByExampleSelective", parms);
        return rows;
    }

    public int updateByExample(CustInfo record, CustInfoCriteria example) {
        UpdateByExampleParms parms = new UpdateByExampleParms(record, example);
        int rows = getSqlMapClientTemplate().update("cust_info.ibatorgenerated_updateByExample", parms);
        return rows;
    }

    public int updateByPrimaryKeySelective(CustInfo record) {
        int rows = getSqlMapClientTemplate().update("cust_info.ibatorgenerated_updateByPrimaryKeySelective", record);
        return rows;
    }

    public int updateByPrimaryKey(CustInfo record) {
        int rows = getSqlMapClientTemplate().update("cust_info.ibatorgenerated_updateByPrimaryKey", record);
        return rows;
    }

     private static class UpdateByExampleParms extends CustInfoCriteria {
        private Object record;

        public UpdateByExampleParms(Object record, CustInfoCriteria example) {
            super(example);
            this.record = record;
        }

        public Object getRecord() {
            return record;
        }
    }
}