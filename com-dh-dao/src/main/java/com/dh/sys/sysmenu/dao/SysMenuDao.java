package com.dh.sys.sysmenu.dao;

import com.dh.sys.sysmenu.model.SysMenu;
import com.dh.sys.sysmenu.model.SysMenuCriteria;
import java.util.List;

public interface SysMenuDao {

    int countByExample(SysMenuCriteria example);


    int deleteByExample(SysMenuCriteria example);


    int deleteByPrimaryKey(String id);


    void insert(SysMenu record);


    void insertSelective(SysMenu record);


    List<SysMenu> selectByExample(SysMenuCriteria example,Integer page, Integer rows);


    SysMenu selectByPrimaryKey(String id);


    int updateByExampleSelective(SysMenu record, SysMenuCriteria example);


    int updateByExample(SysMenu record, SysMenuCriteria example);


    int updateByPrimaryKeySelective(SysMenu record);


    int updateByPrimaryKey(SysMenu record);

    List<SysMenu> selectByExample(SysMenuCriteria example);

    
}