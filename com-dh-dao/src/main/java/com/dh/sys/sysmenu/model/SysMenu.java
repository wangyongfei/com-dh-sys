package com.dh.sys.sysmenu.model;

import java.io.Serializable;

public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
    * 主键
    */
    private String id;


    /**
    * 名称
    */
    private String name;


    /**
    * 图片地址
    */
    private String imageUrl;


    /**
    * 
    */
    private String url;


    /**
    * 提示语
    */
    private String qtip;


    /**
    * 
    */
    private Integer leaf;


    /**
    * 
    */
    private String parentNode;


    /**
    * 
    */
    private String createDate;


    /**
    * 是否启用 0:未启用 1:已启用
    */
    private Integer flag;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getQtip() {
        return qtip;
    }

    public void setQtip(String qtip) {
        this.qtip = qtip;
    }
    
    public Integer getLeaf() {
        return leaf;
    }

    public void setLeaf(Integer leaf) {
        this.leaf = leaf;
    }
    
    public String getParentNode() {
        return parentNode;
    }

    public void setParentNode(String parentNode) {
        this.parentNode = parentNode;
    }
    
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    
    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    

}