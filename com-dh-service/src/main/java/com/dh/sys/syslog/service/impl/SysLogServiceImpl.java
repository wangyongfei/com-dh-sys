package com.dh.sys.syslog.service.impl;


import com.dh.sys.syslog.dao.SysLogDao;
import com.dh.sys.syslog.model.SysLog;
import com.dh.sys.syslog.model.SysLogCriteria;
import com.dh.sys.syslog.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("SysLogService")
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogDao dao;
    //
    public List<SysLog> select(SysLog record){

        SysLogCriteria example = new SysLogCriteria();

        List<SysLog> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(SysLog record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(SysLog record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<SysLog> findAllListPage(SysLog record, Integer page, Integer rows) {

        SysLogCriteria example = new SysLogCriteria();

        List<SysLog> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(SysLog record) {
        SysLogCriteria example = new SysLogCriteria();
        // todo
        return dao.countByExample(example);
    }

    public SysLog selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(SysLog record) {
        dao.insert(record);
    }

    @Override
    public List<SysLog> select(String startDate, String endDate) {
        SysLogCriteria example = new SysLogCriteria();
        SysLogCriteria.Criteria criteria = example.createCriteria();
        criteria.andCreateDateBetween(startDate,endDate);
        return dao.selectByExample(example);
    }
}