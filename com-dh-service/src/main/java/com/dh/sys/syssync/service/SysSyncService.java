package com.dh.sys.syssync.service;

import com.dh.sys.syssync.model.SysSync;
import java.util.List;

public interface SysSyncService {

    public List<SysSync> select(SysSync record);

    public boolean delete(SysSync record);

    public boolean update(SysSync record);

    List<SysSync> findAllListPage(SysSync record, Integer page, Integer rows);

    int count(SysSync record);

    SysSync selectById(String id);

    void save(SysSync sm);

    List<SysSync> findSysSyncData(List<Integer>unStatus, int status);

}