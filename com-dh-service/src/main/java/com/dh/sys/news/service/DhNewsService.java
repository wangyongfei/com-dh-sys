package com.dh.sys.news.service;

import com.dh.sys.news.model.DhNews;

import java.util.List;

public interface DhNewsService {

    public List<DhNews> select(DhNews record);

    public boolean delete(DhNews record);

    public boolean update(DhNews record);

    List<DhNews> findAllListPage(DhNews record, Integer page, Integer rows);

    int count(DhNews record);

    DhNews selectById(String id);

    void save(DhNews sm);

    public void saveRedis(final DhNews record);

    public DhNews getByKey(final String keyId);

}