package com.dh.sys.mongo;

import com.dh.sys.sync.MongosyncEntity;
import com.dh.sys.syslog.model.SysLog;
import com.mongodb.DBCollection;
import com.mongodb.gridfs.GridFSFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Administrator on 2016/8/20.
 */
@Service
public class MongoService {

    @Autowired
    private MongoDao mongoDao;

    public List<FilePicsEntity> findList(int skip, int limit) {
        return mongoDao.findList(skip, limit);
    }

    public List<FilePicsEntity> findAllList() {
        return mongoDao.findAllList();
    }

    public void save(Object filePicsEntity) {
        mongoDao.save(filePicsEntity);
    }

    /**
     * 保存图片
     * @param inputStream
     * @param fileName
     * @return
     */
    public GridFSFile store(InputStream inputStream, String fileName){
        return mongoDao.store(inputStream, fileName);
    }

    /**
     *删除图片
     * @param fileName
     */
    public void removeFile(String fileName ){
        mongoDao.removeFile(fileName);
    }

    public void remove(String id) {
        mongoDao.remove(id);
    }

    public FilePicsEntity findById(String id) {
        return mongoDao.findById(id);
    }

    /**
     * mongo批量插入最大数据支持48M
     * @param list
     * @param collectionName collection  字符串
     */
    public void batchInsertByCollectionName(List<?>list,String collectionName){
        mongoDao.batchInsertByCollectionName(list, collectionName);
    }


    /**
     *  mongo批量插入最大数据支持48M
     * @param list
     * @param clazz collection 类
     */
    public void batchInsertByCollectionClass(List<?>list,Object clazz){
        mongoDao.batchInsertByCollectionClass(list, clazz.getClass());
    }

    /**
     * collection 字符串
     * 创建collection
     * @param collectionName
     * @return
     */
    public DBCollection createCollectionName(String collectionName){
        return mongoDao.createCollectionName(collectionName);
    }

    /**
     * collection 类
     * 创建collection
     * @param clazz
     * @return
     */
    public DBCollection createCollectionClass(Object clazz){
        return mongoDao.createCollectionClass();
    }

    public List<?> findAllList(String clooectionName) {
        return mongoDao.findAllList(clooectionName);
    }

    public void findAllList(SysLog sysLog1, String collectionName) {
//        return mongoDao.findAllList(sysLog1,collectionName);
    }
}
