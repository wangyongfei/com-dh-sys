package com.dh.sys.sysconfigcenter.service.impl;


import com.dh.sys.sysconfigcenter.dao.SysConfigCenterDao;
import com.dh.sys.sysconfigcenter.model.SysConfigCenter;
import com.dh.sys.sysconfigcenter.model.SysConfigCenterCriteria;
import com.dh.sys.sysconfigcenter.service.SysConfigCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("SysConfigCenterService")
public class SysConfigCenterServiceImpl implements SysConfigCenterService {

    @Autowired
    private SysConfigCenterDao dao;
    //
    public List<SysConfigCenter> select(SysConfigCenter record){

        SysConfigCenterCriteria example = new SysConfigCenterCriteria();

        List<SysConfigCenter> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(SysConfigCenter record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(SysConfigCenter record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<SysConfigCenter> findAllListPage(SysConfigCenter record, Integer page, Integer rows) {

        SysConfigCenterCriteria example = new SysConfigCenterCriteria();

        List<SysConfigCenter> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(SysConfigCenter record) {
        SysConfigCenterCriteria example = new SysConfigCenterCriteria();
        // todo
        return dao.countByExample(example);
    }

    public SysConfigCenter selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(SysConfigCenter record) {
        dao.insert(record);
    }
}