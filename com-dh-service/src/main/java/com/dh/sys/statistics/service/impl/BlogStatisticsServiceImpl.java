package com.dh.sys.statistics.service.impl;


import com.dh.sys.statistics.dao.BlogStatisticsDao;
import com.dh.sys.statistics.model.BlogStatistics;
import com.dh.sys.statistics.model.BlogStatisticsCriteria;
import com.dh.sys.statistics.service.BlogStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("BlogStatisticsService")
public class BlogStatisticsServiceImpl implements BlogStatisticsService {

    @Autowired
    private BlogStatisticsDao dao;
    //
    public List<BlogStatistics> select(BlogStatistics record){

        BlogStatisticsCriteria example = new BlogStatisticsCriteria();

        List<BlogStatistics> selectByExample = dao.selectByExample(example);

        return selectByExample;
    }

    public boolean delete(BlogStatistics record){
        try {
            int i = dao.deleteByPrimaryKey(record.getId());
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(BlogStatistics record){
        try {
            int i = dao.updateByPrimaryKey(record);
            return i>0?true:false;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public List<BlogStatistics> findAllListPage(BlogStatistics record, Integer page, Integer rows) {

        BlogStatisticsCriteria example = new BlogStatisticsCriteria();

        List<BlogStatistics> selectByExample = dao.selectByExample(example,page,rows);

        return selectByExample;

    }

    public int count(BlogStatistics record) {
        BlogStatisticsCriteria example = new BlogStatisticsCriteria();
        // todo
        return dao.countByExample(example);
    }

    public BlogStatistics selectById(String id) {
        return dao.selectByPrimaryKey(id);
    }

    public void save(BlogStatistics record) {
        dao.insert(record);
    }
}