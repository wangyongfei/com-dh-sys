package com.dh.sys.statistics.service;

import com.dh.sys.statistics.model.BlogStatistics;
import java.util.List;

public interface BlogStatisticsService {

    public List<BlogStatistics> select(BlogStatistics record);

    public boolean delete(BlogStatistics record);

    public boolean update(BlogStatistics record);

    List<BlogStatistics> findAllListPage(BlogStatistics record, Integer page, Integer rows);

    int count(BlogStatistics record);

    BlogStatistics selectById(String id);

    void save(BlogStatistics sm);

}